# SPHEREx photo-z
The photometric redshift estimation pipeline for SPHEREx


## A Guided Tour

The directory structure and the most important files are summarized below.

### / (root directory)

#### subdirectories
| Path            | Contents |
| :---            | :---     |
| /3rdpartylibs/  | Third-party libraries, such as Boost, Folly, FFTW, and GSL     |
| /GDL/           | GDL-specific files needed to run the IDL scripts with GDL      |
| /IMCAT-bin/     | x86-64 Linux IMCAT binaries                                    |
| /input/         | Input catalog data, filters, and templates                     |
| /RUNS/          | Individual simulations, configurations files, and output files |
| /src/           | Source files and scripts                                       |  

#### noteworthy files

| Name           | Description    | Usage / Notes|
| :---           | :---           | :--- |
| NewSimulation.sh |  Sets up a new simulation                |  `./NewSimulation.sh <simulation_name>` |
| README.md      | Overview documentation in Markdown format  | Compile to HTML with `pandoc README.md --toc --css style.css -o README.html`, assuming that pandoc is installed.


### /src/

#### subdirectories
| Path         | Contents |
| :---           | :---     |
| /src/cpp/| The C++ source files  for photo-z, make-grid, and fitcat |
| /src/scripts/ | Bash, Python, and Perl scripts used by the simulation |

#### noteworthy files

| Name | Description     | Usage / Notes |
| :------------- | :------------- | :----------- |
| Astyle.style      | Style definition file for the source code formatter, astyle  | ``$ astyle --options=Astyle.style -r "cpp/*.hpp" "cpp/*.cpp"``|
| photoz.creator | Qt Creator project file for the C++ component of the simulation | Opening this project file with the Qt Creator IDE will allow the user to quickly study the code an begin editing. |
| photoz.files| A list of all source and header files in the project | This is automatically read by Qt Creator when the project is loaded |
| photoz.includes | A list of search paths for libraries that are not installed at the system level (i.e., the third party libraries) | This is automatically read by Qt Creator when the project is loaded |


### /input/

#### subdirectories
| Name           | Contents |
| :---           | :---     |
| /input/FILTER/ | Filter transmission files |
| /input/SED/    | SED template files |

#### noteworthy files

| Name     | Description    |
| :------------- | :------------- |
| COSMOS.cat     | COSMOS catalog containing angular positions, masses, redshifts, and flux data for the COSMOS bands.|
| COSMOS.in      | The default input catalog for the simulation.  |
| model_grid_spec.in | The default model grid specification file. |

### /src/cpp/

| Name | Description     | Usage / Notes |
| :------------- | :------------- | :----------- |
| build.sh | A simple build script (which should be replaced with a Makefile) | Run `$ ./build.sh` to compile the programs. |
| fit_catalog.cpp | Contains main() for the catalog photometry fitting code, fitcat | See [fitcat documentation](#fitcat-documentation) |
| make_grid.cpp | Contains main() for the simulated photometry generator, make-grid| See [make-grid documentation](#make-grid-documentation) |
| photo_z.cpp | Contains main() for the redshift estimation program, photo-z | See [photo-z documentation](#photo-z-documentation)|


## Getting Started

These instructions assume that you are using a modern Debian derivative, such as Ubuntu.

#### Step 1: *Install Dependencies*

If you wish to simply run the simulations, install the necessary packages, using the command:

    $ sudo apt-get install build-essential
      binutils \
      python-numpy \
      python-matplotlib \
      python-pyfits \
      perl \
      perl-modules \
      pdl \
      gnudatalanguage

If you plan to edit the code, you should also install these:

    $ sudo apt-get install qtcreator \
      doxygen \
      pandoc \
      astyle

#### Step 2: *Compile*

Navigate into the /src/cpp/ directory and compile the binaries using:

    $ ./build.sh

#### Step 3: *Link IDL to GDL*

If you do not have IDL installed, run the following command to make GDL appear to be IDL:

    $ sudo ln -s /usr/bin/gdl /usr/bin/idl

#### Step 4: *Create New Simulation*

Navigate to the root directory of the SPHEREx pipeline project and start a new simulation by running

    $ ./NewSimulation.sh <simulation name>

Where `<simulation name>` is the name that you have chosen for the simulation that you wish to run.

#### Step 5: *Customize New Simulation*

Navigate to the RUNS/<simulation name> directory, edit the parameters in the SPHEREx.params file. Be sure
to place a copy of the desired model grid specification file and the input catalog file into the simulation
directory (The full COSMOS catalog is probably what you want to use for the input catalog: /input/COSMOS.in.
   The default grid specification is /input/model_grid_spec.in. Custom grid specifications can be generated
   using /src/scripts/MakeGridSpec.py). You will also probably want to copy the template probability file.

#### Step 6: *Run*

Try to run the simulation:

    $ ./RunSimulation.sh

## Documentation for binaries

For the most detailed documentation of the binaries, see the source documentation at
[Docs/html/index.html](Docs/html/index.html) and the source itself. For usage information,
read the following:

### fitcat Documentation

The fitcat program fits catalog photometry values (presented as fluxes, rather than
magnitudes) to a set of SED templates. The basic procedure works as follows:

0) Before you start using fitcat, sort your input catalog by redshift (perhaps using /src/scripts/sort-by-column.py).
1) Split the catalog into small partitions, which each contain a small number of unique redshifts.
2) For each partition of the catalog, create a grid of models with redshift values that match the
   redshifts of objects present in the input catalog.
3) Use the grid to fit the output the template id, E(B-V), reddening law, and normalization for each object in the catalog.
4) Concatenate the output files into a single catalog
5) Optionally, sort the output catalog by object ID so that the entries in the output file are ordered the same as in the input catalog.

An incomplete script for performing the actions outlined above is stored in src/scripts/FitCatalog.sh.
Although it is incomplete and untested, it outlines the main steps more concretely than the summary provided here.

To create the grid for each partition:

    $ ./fitcat mkgrid <catalog> <filter_list> <template_list> <output file>

Where `<catalog>` is a partition of the catalog file, `<filter_list>` is a list of
filter transmission files that correspond to the fluxes provided in the catalog,
`<template_list>` is a list of SED templates that you wish to fit to the photometry,
and `<output file>` is simply the name of the output file that will contain the model
grid for this partitiion of the catalog.

To fit the photometry to the model grid:

    $ ./fitcat fit <catalog> <grid> <final output file>

Where `<catalog>` is the same as above, `<grid>` is the output file from the grid
generation step, and `<final output file>` is the name of the resulting output file,
which will contain template ids, E(B-V), reddening law, and normalization values that
were determined to best fit the input photometry.

### make-grid Documentation

The make-grid program integrates filters against templates to produce simulated
photometry measurements for a set of objects.

Syntax:

    $ make-grid <template list> <filter list> <input catalog> <output filename>

Where...

i) the `template list` is an ASCII file containing the names of SED template files, with one filename per line.
ii)  the `filter list` is ASCII file containing the names of filter transmission files, with one filename per line.
iii) the `input catalog` is an ASCII file containing the properties of the objects whose photometry will be simulated.
   In certain circumstances, this file is also referred to as the "model grid specification file," when it is used
   to create a model grid that will be used by the photo-z fitting code. The input catalog must contain
   the following columns:

    1) Object ID number
    2) RA (right ascension)
    3) DEC (declination)
    4) I-band magnitude
    5) Redshift
    6) Template ID number
    7) E(B-V)
    8) reddening law
    9) Normalization (factor by which to multiply the template)
    10) Emission line scaling factor

  iv) the `output filename` is the the desired /path/and/filename of the output file.
     If the filename ends with the characters `.photoz`, the output file is saved
     in a binary file format that can be read by the photo-z code. If the output
     file ends with the characters `.template`, the output file will contain the
     wavelengths and fluxes of the modified templates (SEDs); integration with filters is not
     performed. In all other cases, the output file is an ASCII file containing
     the input catalog data, plus fluxes associated with each filter.

### photo-z Documentation

The photo-z program performs the chi-squared comparisons between the observed fluxes
and fluxes in a 'model grid', which is a file generated by the make-grid program
which contains many pre-computed SED models.

Syntax:

    $ photo-z <model grid> <observed SEDs> <template probabilities> <output filename> [pdfs file]

Where...

i)   the `model grid` is a file (or group of files) generated using the make-grid program.
    Model grid files are binary files with filenames that end in `.photoz`. If the model grid
    is distributed over a collection of files, then you typically want to use `'/path/to/files/pattern*.photoz'`
    as the filename, noting that the quotes around the pattern are necessary to prevent shell
    expansion of the wildcard character (photo-z performs the pattern matching itself.
ii)  the `observed SEDs` is an ASCII file
iii) the `template probabilities` is an ASCII file
iv)  the `output filename` is the name of the file which will contain the output data generated by photo-z.
v)   the `pdfs file` argument (optional) is the /path/and/filename of a file that
     will contain the entire redshift probability distribution for each object in
     the `observed SEDs` file. Caution: This file is potentially enormous.


## Driver scripts

### SPHEREx.params

SPHEREx.params is the parameter file containing most of the adjustable parameters
that are used to customize specific simulations. In addition to setting basic parameters,
this file sets up the environment so that all of the programs involved in the simulation
will work properly. When a new simulation run is initialized by calling NewSimulation.sh,
the template at /src/scripts/SPHEREx.params.template is used to create a default
SPHEREx.params file. The user should always carefully examine and edit this file before
running a new simulation.

### RunSimulation.sh

This is the main driver script of the simulation. It is analogous to the main()
function in a C program. There are two main code paths in the script:

1) When StratOS is detected, the script copies data to the distributed file system
   and runs the most computationally intensive tasks on the worker nodes of the
   StratOS cluster. Whenever possible, the less computationally intensive parts
   of the simulation are executed on the login node while the worker nodes are
   performing other tasks.

2) When StratOS is not detected, all tasks are run on the local machine.

### MakeNoiseModel.sh

Extracts data from the hardware sensitivity model script and creates LVF filter
transmission files.

### MakeSimulatedPhotometry.sh

Uses make-grid to simulate the photometry of the objects in the input catalog. The
templates listed in the input catalog are integrated against the LVFs in order to
create simulated SEDs. The output is used to create images for each LVF.

### NormalizePhotometry.sh

The title of this is a misleading because it hasn't been re-named appropriately.
The script re-organizes flux data so that it can be used in the next step to create
images for the SPHEREx LVF bands. It also saves a catalog of input data.

### MakeImage.sh

This script creates simulated images for each of the LFVs (a FITS file for each
LVF). This runs on the login node in all cases (i.e., it is not executed on the
slave nodes of the cluster).

### SetupPhotometry.sh

Performs tasks required to begin measuring the photometry from the images that
were generated by MakeImage.sh.

### MeasurePhotometry.sh

Measures fluxes from the FITS images generated by MakeImage.sh and creates a catalog
of SEDs that can be read by the redshift estimation code, photo-z.

### MakeModelGrid.sh

Uses make-grid to create model grid that will later be used by the photo-z code.
If StratOS is detected, the grid is generated on the slave nodes while
NormalizePhotometry.sh, MakeImage.sh, SetupPhotometry.sh, and MeasurePhotometry.sh
run on the login node.

### RunPhoto-z.sh

Runs the photometric redshift estimation code, photo-z. If StratOS is detected, the job
is distributed over the nodes of the cluster.

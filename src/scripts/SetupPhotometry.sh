#! /bin/bash

# Prepares to perform photometry using known positions of objects

source ./SPHEREx.params
source utils.sh

errlog "\nPreparing to measure photometry."

# set up the WCS FITS file header (hard-coded to the center of COSMOS)
# note: this is also hard-coded into make_full_simim.pl

InstrumentRes=$(python -c "print ( int(1.4 * 3600 / $PixSizeInArcsec) )")

HiRes=$(($ResamplingFactor * $InstrumentRes))

center=$(($HiRes / 2))

CRpix=$(($center / $ResamplingFactor))

dRA=$(python -c "print(-1.0 * $PixSizeInArcsec / 3600.0)")

dDEC=$(python -c "print($PixSizeInArcsec / 3600.0)")

echo "InstrumentRes is $InstrumentRes"
echo "HiRes is $HiRes"
echo "center is $center"
echo "CRpix is $CRpix"
echo "dRA is $dRA"
echo "dDEC is $dDEC"

errlog "Running command: ic -c $InstrumentRes $InstrumentRes '0' | setwcsinfo RA---STG $dRA 150.11916667 $CRpix DEC--STG $dDEC 2.20583333 $CRpix 0.0 -c -e 2000.0 2000.0 > wcs.fits\n"

ic -c $InstrumentRes $InstrumentRes '0' | setwcsinfo RA---STG $dRA 150.11916667 $CRpix DEC--STG $dDEC 2.20583333 $CRpix 0.0 -c -e 2000.0 2000.0 > wcs.fits

xmax=$InstrumentRes
ymax=$InstrumentRes

# begin constructing command to convert RA and DEC to pixel coordinates and 
# set flux limits based on expected depth of data for each broad-band filter
# This basically clips the data brighter than a limit.

cmd="getimcoords wcs.fits < $ModelParamCatalog | lc -i '%r[0] 3 > %r[1] 3 > and %r[0] $xmax 3 - < and %r[1] $ymax 3 - < and ' -b ID 'flux = %meanflux' 'ID2 = 0' 'flux2 = 0' 'NID = 0' 'Nflux = 0' 'Edet = 1' +all | lc -i '"

DepthIdx=0

for filter in ${FilterNames[@]}; do

	limit=$(python -c "print ( 10**(-0.4 * (${ExternalDataDepth[DepthIdx]} - 23.9)) )")

	cmd="${cmd}%${filter} ${limit} "

    if [[ $DepthIdx > 0 ]]; then #&& [[ $DepthIdx != $(( ${#FilterNames[@]} - 1)) ]]; then
        cmd="${cmd} >= or "
    elif [[ $DepthIdx == 0 ]]; then
    	cmd="${cmd} >= "
    fi

	DepthIdx=$((DepthIdx + 1))

done

cmd="$cmd '"

echo $cmd
# launch the previously-constructed command and save output to the InputPhotometry file

eval $cmd > $InputPhotometry

# generate a file specifying how many objects fall within each pixel
# so that it's possible to flag pixels that contain more than one object

errlog "Generating photometry image, $PhotometryImg"

cat $InputPhotometry | makedensity r 0 $InstrumentRes $InstrumentRes \
                                     0 $InstrumentRes $InstrumentRes > $PhotometryImg

errlog "Creating catalog of positions, $PositionCatalog"

# get ID, RA, DEC, Redshift, and (x, y) for all objects
cat $InputPhotometry | lc -o ID RA DEC redshift r > $PositionCatalog

errlog "Adding noise to data and saving as $PhotometryData"

# add Gaussian noise to the ground data
# used for fitting later (really should add the noise first and then cut)
add_noise_to_external.pl $InputPhotometry > $PhotometryData

errlog "Finished setting up the photometry measurement."

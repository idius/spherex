#! /bin/bash

# ==================== MakeModelGrid ====================

# computes the model grid. This is expensive so we only do it if the filters,
# templates, or wavelength range has changed. Don't run this if only the noise changed.

source ./SPHEREx.params
source utils.sh

errlog "Generating the model grid."

# if the output file already exists and it is not older than the $ModelGridSpec and 
# $TemplateLibrary files, then prompt the user: do you really want to generate a new grid?

if test -e "$ModelGrid" \
    && test "$ModelGridSpec" -ot "$ModelGrid" \
    && test "$TemplateLibrary" -ot "$ModelGrid"; then

    echo "Note: the file $ModelGrid already exists and it is newer than the input files. "
    # todo use read to get feedback from the user here
fi

if [ "$STRATOS_HOME" != "" ]; then

    # StratOS has been detected. Split the file into shards, submit the command to the 
    # cluster, then combine the output files. TODO: do the splitting automatically 
    # with the StratOS partitioner and recombine, automatically.

    errlog "Copying filters and templates to the DFS: $DFS_DIR"

    cp -r $FilterDir $DFS_DIR/

    cp -r $TemplateDir $DFS_DIR/

    shard_base="${ModelGridSpec}.part-"

    split -d -n l/$StratOS_Shards $ModelGridSpec $shard_base

    errlog "Copying $ModelGridSpec to the DFS: $DFS_DIR"

    cp ${shard_base}* $DFS_DIR/

    # make sure that the files are really written to disk (not cached)

    ansible all -a "sync"

    stratos-run $STRATOS_HOME/bin/make-grid $DFS_DIR/$TemplateLibrary \
                                            $DFS_DIR/$FittingDataFilterList \
                                            "%c% %c%.photoz" \
                                            "$DFS_DIR/$shard_base*"

    errlog "Writing the model grid in $(ls $DFS_DIR/*.photoz)"

    rm ${shard_base}*

else
    # run locally
    make-grid $TemplateLibrary $FittingDataFilterList $ModelGridSpec ${ModelGrid}.photoz
fi

if [[ $? == 0 ]]; then
    # TODO: verify that the output file exists and is non-empty.
    errlog "Finished generating the model grid."
else
    errlog "Grid generation failed."
    exit 1
fi

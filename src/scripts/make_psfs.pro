pro make_psfs, R40_file, outputfile

;Using the Gaussians provided by Peter, make the psfs for the optimal
;extraction. 9x9 arrays with pixels sized 6.2/3 arcsec. Element
;(8,8) will be the nominal center.

readcol,R40_file,filts40, chan40, res40, cent40, fw40, fluxerr40, psf_sigma, f='a,a,d,d,d,d,d'

temp = {psf:dblarr(252,252)+1, filter:" "}
psf_struct = replicate(temp, n_elements(filts40))
pixscale = 6.2/63.

for filt = 0, n_elements(filts40) - 1 do begin
   temparr = dblarr(252,252)+1

   for i = 0, 251 do begin
      for j = 0,251 do begin
         temparr[i,j] = exp(-(((i-126.)*pixscale)^2 + ((j-126.)*pixscale)^2)/(2*(psf_sigma[filt])^2))
      endfor
   endfor
   ;normalize the result
   psf_struct[filt].psf = temparr/total(temparr)
endfor
 
mwrfits, psf_struct, outputfile, /create

print, "step 4"

end;

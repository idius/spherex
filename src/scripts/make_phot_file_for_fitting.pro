pro make_phot_file_for_fitting, optimalphot_file, ground_data_file, outputfile

;use fits file with photometry to make the SIMAP input
rr = mrdfits(optimalphot_file,1)

readcol,ground_data_file,id,ra,dec,redshift,g,dg,r,dr,i,di,z,dz,y,dy,W1,dW1,W2,dW2, f='(L,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d)

openw, lun, outputfile, /get_lun


for ii = 0L, n_elements(rr)-1 do begin
  ;printf, lun, id[ii], Format='(I)'
  printf, lun, id[ii], rr[ii].xpos, rr[ii].ypos,Format='(I9,2X,E10.2,E10.2,$)'
  printf, lun, g[ii],dg[ii],r[ii],dr[ii],i[ii],di[ii],z[ii],dz[ii],y[ii],dy[ii],W1[ii],dW1[ii],W2[ii],dW2[ii], Format='(14E10.2,$)'

  for jj = 0L, n_elements(rr[0].flux)-1 do begin
    if (rr[ii].fluxerr[jj] le 1e-2) then begin
	if (rr[ii].flux[jj] le 1e-2) then begin
   	  printf, lun, 1e-2, 9e9, Format='(E10.2,E10.2,$)'
        endif else begin	
          printf, lun, rr[ii].flux[jj], 1e-2, Format='(E10.2,E10.2,$)'
        endelse
    endif else begin	
	printf, lun, rr[ii].flux[jj], rr[ii].fluxerr[jj], Format='(E10.2,E10.2,$)'
    endelse

  endfor

  printf, lun, '';

endfor
close, lun
free_lun, lun

end;

#! /bin/bash

source ./SPHEREx.params
source utils.sh

errlog "\nPreparing to run the Photo-z fitting code."


if [ "$STRATOS_HOME" != "" ]; then

    # StratOS has been detected. Split the file into shards, submit the command to the 
    # cluster, then combine the output files. TODO: do the splitting automatically 
    # with the StratOS partitioner and recombine, automatically.

    ModelGridGlobPattern="${ModelGridSpec}.part-*.photoz"

    shard_base="${OutputSedCatalog}.part-"

    split -d -n l/$StratOS_Shards $OutputSedCatalog $shard_base

    errlog "Copying $OutputSedCatalog to the DFS: $DFS_DIR"

    cp ${shard_base}* $DFS_DIR/

    errlog "Copying $TemplateProbabilities to the DFS: $DFS_DIR"

    cp $TemplateProbabilities $DFS_DIR/

    # make sure that the files are really written to disk (not cached)

    ansible all -a "sync"

    sleep 10 # poor coding here: this is intended to give FUSE-DFS + HDFS time to update. 

    #TODO: possibly check to make sure that all nodes are able to read all of the shards.

    #counts=($(ansible all -m shell -a "head $DFS_DIR/${shard_base}* | wc -w" | grep -E '^[0-9]+'))

    sleep 3

    stratos-run $STRATOS_HOME/bin/photo-z "$DFS_DIR/$ModelGridGlobPattern" \
                                          "%c% $DFS_DIR/$TemplateProbabilities %c%.output" \
                                          "$DFS_DIR/$shard_base*"

    errlog "Saving estimated redshifts in $DFS_DIR/$OutputRedshiftData"

    cat $DFS_DIR/${shard_base}*.output > $DFS_DIR/$OutputRedshiftData

    rm $DFS_DIR/${shard_base}* ${shard_base}*

    errlog "Copying $OutputRedshiftData to simulation directory."

    cp $DFS_DIR/$OutputRedshiftData .

else
    # run locally

    echo "photo-z ${ModelGrid}.photoz $OutputSedCatalog $TemplateProbabilities $OutputRedshiftData"

    photo-z "${ModelGrid}.photoz" $OutputSedCatalog $TemplateProbabilities $OutputRedshiftData
fi

errlog "Creating the final catalog."

cat $OutputRedshiftData | lc -C -n oID -N '1 2 x' -n pz -n dpz -n skew -n kurt -n pdfsum -n pdfmax -n mlz -n mlch2 -n minchi2 -n meanchi2 -n mltemp -n mintemp -n meanflx -n wmeanflx -n rmsflx -n snr0 -n snr1 -n snr2 -n snr3 > $OutputRedshiftCatalog

pastecats $InputPhotometry $OutputRedshiftCatalog > $SPHERExOutputCatalog

errlog "The final simulated catalog output has been saved in $SPHERExOutputCatalog"

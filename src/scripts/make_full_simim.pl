#!/usr/bin/perl

$resam="$ENV{'ResamplingFactor'}";
$pixSize="$ENV{'PixSizeInArcsec'}";
$Ferr="$ENV{'FractionalError'}";

$usage = "$0 incat\n";
$incat = shift(@ARGV) || die $usage;

$Npix=1.4*3600/$pixSize;
$Npix=int($Npix);
$HRpix=$Npix*$resam;
$cen=$HRpix/2;
$sf=3600*$resam/$pixSize;

$CRpix = $cen/$resam;

$Nrand=$Npix*$Npix;
$HRdRA = -1.0*$pixSize/(3600*$resam);
$HRdDEC = $pixSize/(3600*$resam);

$dRA = -1.0*$pixSize/(3600);
$dDEC = $pixSize/(3600);

$pid=$$;

printf STDERR "incat is $incat\n";

system("mkdir -p simim rawim noiseim");
system("makegridcat $Npix $Npix | lc -b > $pid.grid.cat");

printf STDOUT "Np = %d HRpix = %d cen = %lf sf = %lf\n",$Npix,$HRpix,$cen,$sf;

system("ic -c $HRpix $HRpix '0' | setwcsinfo RA---TAN $HRdRA 150.11916667 $cen DEC--TAN $HRdDEC 2.20583333 $cen 0.0 -c -e 2000.0 2000.0 | imhead -b > $pid.wcs.fits");

open(FIL,"< $incat");

$i=0;
while(<FIL>){
  chomp;
  ($filter,$CHname,$R,$l,$dl,$noise,$PSFsigma)=split;

  $outfits = "./rawim/" . $CHname . "-R" .$R . "." . $filter . ".flux.fits";
  $noiseim = "./noiseim/" . $CHname . "-R"  .$R . "."  . $filter . ".noise.fits";
  $fnoise = "./noiseim/" . $CHname . "-R"  .$R . "."  . $filter . ".fnoise.fits";

  $fnum = $filter;
  $fnum =~ s/F//g;

  #scale up the PSF;
  $smkern = $PSFsigma*$resam/$pixSize;

  printf STDERR "Doing Filter %s %s %d %lf %lf\n",$CHname,$filter,$R,$noise,$smkern;

  $catfile = "$ENV{'SimulationCatalogDir'}/R" . $R . "-" . $CHname  . "-" . $fnum . ".txt";

  system("lc -C -n ID -n RA -n DEC -n z -n f < $catfile | lc -i '%z 9 < %z 0 > and %f 0 > and %f 1e6 < and' -b RA DEC f | getimcoords $pid.wcs.fits | makedensity r 0 $HRpix $HRpix 0 $HRpix $HRpix -v f  | smooth -g $smkern $smkern 0.0 > $outfits");
  
  $seed = rand(100000);
  $seed = int($seed);

  system("makerandcat $Nrand -g -dim 1 -b -seed $seed | lc -b 'f = %x' > $pid.rand.cat");
  system("pastecats $pid.grid.cat $pid.rand.cat | makedensity x 0 $Npix $Npix 0 $Npix $Npix -v f | ic '%1 $noise *' - > $noiseim");
  
  srand();

  $seed = rand(100000);
  $seed = int($seed);

  system("makerandcat $Nrand -g -dim 1 -b -seed $seed | lc -b 'f = %x' > $pid.rand.cat");
  system("pastecats $pid.grid.cat $pid.rand.cat | makedensity x 0 $Npix $Npix 0 $Npix $Npix -v f | ic '%1 $Ferr * 1 +' - > $fnoise");
  
  $outsim = "./simim/" . $CHname . "-R"  .$R . "." . $filter . "-" . "$ENV{'MissionName'}" . ".sim.fits";
    
  system("ic '%1 $resam * $resam *' $outfits | scrunch | scrunch | scrunch |  ic '%1 %2 * %3 1.0 * +' - $fnoise $noiseim | setwcsinfo RA---STG $dRA 150.11916667 $CRpix DEC--STG $dDEC 2.20583333 $CRpix 0.0 -c -e 2000.0 2000.0 > $outsim");
  printf STDERR "Done %s\n", $outsim;
}

system("rm $pid.grid.cat $pid.wcs.fits $pid.rand.cat")

#!/usr/bin/python

import sys,getopt
import numpy  as np
import pylab as pl
import simap_util as sut

opts,args = getopt.getopt(sys.argv,"")     
ver=args[1]

Inst = sut.define_instrument(version=ver)
Sens = sut.compute_sensitivities(Inst=Inst)#,Printout=0)

for I in range(len(Sens['lam'][0])):
  print Sens['lam'][0][I],Sens['dF_sh'][0][I],Sens['N_eff'][0][I],Sens['lam_band'][0][I]

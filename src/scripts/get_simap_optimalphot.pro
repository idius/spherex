pro get_simap_optimalphot, input_selected_file, psf_file, R40_file, simtype,noiseSF,outputfile 

;Use a catalog of bright objects and do optimal photometry
;extraction.

;Read in object list
readcol,input_selected_file, ID, RA, DEC, zin, xpos, ypos, f='(L,d,d,d,d,d)'

psfs = mrdfits(psf_file,1)

readcol,R40_file,filts40, chan40, res40, cent40, fw40, fluxerr40, psf_sigma, f='a,a,I,d,d,d,d'

;Scale the noise for this case
fluxerr40=fluxerr40*noiseSF;

ims = {im:fltarr(812,812)}
Nimages = n_elements(filts40)
images = replicate(ims,Nimages)

for ii = 0, Nimages-1 do begin
   thisimname = './simim/'+strcompress(chan40[ii],/remove_all)+ '-R'+strcompress(res40[ii],/remove_all)+ '.' + strcompress(filts40[ii],/remove_all) + '-' + simtype + '.sim*.fits'
   print,thisimname

   thisim = mrdfits(thisimname)

   images[ii].im = thisim
endfor

sz = size(images[0].im)

numobjs = n_elements(ID)
;create structure to hold the photometry
struct = {ID:0L,filtname:strarr(Nimages),filtwave:dblarr(Nimages),dwave:dblarr(Nimages),flux:dblarr(Nimages), $
          fluxerr:dblarr(Nimages),abmag:dblarr(Nimages),dabmag:dblarr(Nimages),xpos:0,ypos:0}
photstruct = replicate(struct, numobjs)

var = {vararr:dblarr(3,3)}
varstruct = replicate(var,Nimages)
for j = 0L, Nimages-1 do begin
   temparr = dblarr(3,3)+fluxerr40[j]^2.;verify this; seems right
   varstruct[j].vararr = temparr
endfor

fluxes={flux:dblarr(Nimages)}
fluxdirect = replicate(fluxes,numobjs)

for obj = 0L, numobjs-1 do begin
   ;speed up processing by putting big if
   ;statement around this, skip really
   ;faint things
   ;if mean(images[*].im[floor(xpos[obj]),floor(ypos[obj])]/fluxerr40) ge 2 then begin ;median S/N greater than 2 across filters 

   thisxpix = floor(xpos[obj]*63.)
   thisypix = floor(ypos[obj]*63.)
   
   ;grab the 9x9 regions
   if thisxpix ge 127 and thisypix ge 127 and thisxpix le 811.*63-127 and thisypix le 811.*63-127 then begin ;avoid edge objects

       ;add some code to mask neighboring
       ;pixels with a bright object, May 12, 2014
       thisxposition = floor(xpos[obj])
       thisyposition = floor(ypos[obj])
       psfmask = dblarr(3,3)+1
       for j1 = 0,2 do begin
          for j2 = 0,2 do begin
             if j1 ne 1 or j2 ne 1 then begin
                idx = where(xpos ge thisxposition+(j1-1) and xpos le thisxposition+j1 and ypos ge thisyposition+(j2-1) and ypos le thisyposition+j2,cnt)
                if cnt gt 0 then psfmask[j1,j2] = 0
             endif            
          endfor
        endfor
        ;end code to mask neighboring objects

      for ii = 0, Nimages-1 do begin
         object_region = images[ii].im[floor(xpos[obj])-1:floor(xpos[obj])+1,floor(ypos[obj])-1:floor(ypos[obj])+1]
         ;get a better per-pixel variance estimate for this region
         
         variance = varstruct[ii].vararr ;
         ;Do the optimal extraction. To do this properly, compute the 
         ;fraction of flux expected in each pixel, using the model gaussian.
         
         psf_weight_array = dblarr(3,3)
         base_x = floor(xpos[obj])
         base_y = floor(ypos[obj])
         for ipix = 0, 2 do begin
            for jpix = 0, 2 do begin
               ;take edges of pixel
               thisxmin = floor((base_x + (ipix-1))*63.)
               thisxmax = floor((base_x + ipix)*63.)
               thisymin = floor((base_y + (jpix-1))*63.)
               thisymax = floor((base_y + jpix)*63.)
               xrange_low = 126-(thisxpix - thisxmin)
               xrange_high = 126-(thisxpix - thisxmax)-1
               yrange_low = 126-(thisypix - thisymin)
               yrange_high = 126-(thisypix - thisymax)-1
               ;can now get the weight for this pixel
               psf_weight_array[ipix,jpix] = total(psfs[ii].psf[xrange_low:xrange_high,yrange_low:yrange_high])
            endfor
         endfor

         ;apply psfmask to avoid including flux
         ;from bright neighboring objects
         pixkeep = where(psfmask eq 1)
         
         optflux = total(psf_weight_array[pixkeep]*object_region[pixkeep]/variance[pixkeep]) / total(psf_weight_array[pixkeep]^2/variance[pixkeep])
         optfluxvar = 1./total(psf_weight_array[pixkeep]^2/variance[pixkeep]) 
         ;put in some code here to get N_effective
         ;Neff = 1/total(psf_weight_array^2)
         ;if ii eq Nimages-1 then begin
         ;print, 'Neff = '+string(Neff)
         ;endif
         
	 fluxdirect[obj].flux[ii] = images[ii].im[base_x,base_y]
         photstruct[obj].filtname[ii] = filts40[ii]
         photstruct[obj].filtwave[ii] = cent40[ii]
         photstruct[obj].dwave[ii] = fw40[ii]
         photstruct[obj].flux[ii] = optflux
         photstruct[obj].fluxerr[ii] = sqrt(optfluxvar)
         if optflux ge sqrt(optfluxvar) then begin
            photstruct[obj].abmag[ii] = -2.5*alog10(optflux)+23.9
            photstruct[obj].dabmag[ii] = 2.5*.43429*sqrt(optfluxvar)/optflux
         endif else begin ;2 sigma limit on magnitude               
            photstruct[obj].abmag[ii] = 99
            photstruct[obj].dabmag[ii] = -2.5*alog10(2*sqrt(optfluxvar))+23.9
         endelse
      endfor
      ;debugging if median(photstruct[obj].abmag) le 17.5 then stop
      ;stop
      photstruct[obj].ID = ID[obj]
      photstruct[obj].xpos = xpos[obj]
      photstruct[obj].ypos = ypos[obj]
   endif else photstruct[obj].ID = -1
;endif; S/N cut 
endfor

mwrfits, photstruct, outputfile, /create

end; pro

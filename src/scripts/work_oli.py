import numpy as np

llcen = 0.75#5.#0.75 # mu

#
pixSize = 6.2 # arcsec
pixPhysSize = 18. # mu
#===
dmeter = 20./100. # diameter in m
PTerr =  6.#10.# 0.5 # Pointing error in arcsec
OptAber = 4.5 # Optical aberation in mu
#===
# This mimics what is in the code.
ptnsm  = PTerr/pixSize #fwhm
ptnsm /= (2.*np.sqrt(2*np.log(2)))
#
diffsm  = 1.22*llcen*1.e-6*180.*3600./(dmeter*3.14159*pixSize) #fhwm
diffsm /= (2.*np.sqrt(2*np.log(2)))
#
optsm = OptAber/pixPhysSize #optical distortion fwhm
optsm /= (2.*np.sqrt(2*np.log(2)))
#
# fwhm = 2 \sqrt{2 \ln 2 } sigma
#
tot  = np.sqrt(optsm**2+diffsm**2+ptnsm**2)  # This is the sigma in pixel unit.
#
print '====='
print 'Ref wavelength   (mu)    : ', llcen
print '--'
print 'fwhm  smoothing  (arcsec): ', diffsm*(2.*np.sqrt(2*np.log(2)))*pixSize
print 'sigma smoothing  (arcsec): ', diffsm * pixSize
print '--'
print 'fwhm pointing    (arcsec): ', PTerr
print 'sigma pointing   (arcsec): ', ptnsm*pixSize
print '--'
print 'fwhm aberation   (arcsec): ', optsm*(2.*np.sqrt(2*np.log(2)))*pixSize
print 'sigma aberation  (arcsec): ', optsm*pixSize
print '--'
print 'fwhm total       (arcsec): ', tot*(2.*np.sqrt(2*np.log(2)))*pixSize
print 'sigma total      (arcsec): ', tot*pixSize
print '====='

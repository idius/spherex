#! /usr/bin/python

import pyfits
import sys
import numpy as np

fitsfile_name = sys.argv[1]
#output_file = sys.argv[2]

hdulist = pyfits.open(fitsfile_name)

catalog = hdulist[1]

ids = catalog.data['Id_1']

ra = catalog.data['alpha']

dec = catalog.data['delta']

logmass = catalog.data['mass_best']

nbands = catalog.data['NbFilt']

mask = catalog.data['flag_Capak']

redshift = catalog.data['zPDF']

upper_z = catalog.data['zPDF_u68']
lower_z = catalog.data['zPDF_l68']

#redshift = catalog.data['zMinChi2'] # alternative method

# currently no cuts are being made

U_corr      = catalog.data['U_corr']
B_corr      = catalog.data['B_corr']
V_corr      = catalog.data['V_corr']
R_corr      = catalog.data['R_corr']
I_corr      = catalog.data['I_corr']
Zn_corr     = catalog.data['Zn_corr']
Yhsc_corr   = catalog.data['Yhsc_corr']
Y_corr      = catalog.data['Y_corr']
J_corr      = catalog.data['J_corr']
H_corr      = catalog.data['H_corr']
K_corr      = catalog.data['K_corr']
Hw_corr     = catalog.data['Hw_corr']
Kw_corr     = catalog.data['Kw_corr']
IA427_corr  = catalog.data['IA427_corr']
IA464_corr  = catalog.data['IA464_corr']
IA484_corr  = catalog.data['IA484_corr']
IA505_corr  = catalog.data['IA505_corr']
IA527_corr  = catalog.data['IA527_corr']
IA574_corr  = catalog.data['IA574_corr']
IA624_corr  = catalog.data['IA624_corr']
IA679_corr  = catalog.data['IA679_corr']
IA709_corr  = catalog.data['IA709_corr']
IA738_corr1 = catalog.data['IA738_corr1']
IA767_corr  = catalog.data['IA767_corr']
IA827_corr  = catalog.data['IA827_corr']
NB711_corr  = catalog.data['NB711_corr']
NB816_corr  = catalog.data['NB816_corr']
ch1_corr    = catalog.data['ch1_corr']
ch2_corr    = catalog.data['ch2_corr']
ch3_corr    = catalog.data['ch3_corr']
ch4_corr    = catalog.data['ch4_corr']
NUV_corr    = catalog.data['NUV_corr']

magnitudes = [U_corr, B_corr, V_corr, R_corr, I_corr, Zn_corr, Yhsc_corr, Y_corr, J_corr, H_corr,
              K_corr, Hw_corr, Kw_corr, IA427_corr, IA464_corr, IA484_corr, IA505_corr, IA527_corr,
              IA574_corr, IA624_corr, IA679_corr, IA709_corr, IA738_corr1, IA767_corr, IA827_corr,
              NB711_corr, NB816_corr, ch1_corr, ch2_corr, ch3_corr, ch4_corr, NUV_corr]


zero_points = [ 0.01344, 0.14837, -0.11423, -0.00912, 0.01994, -0.08934, -0.11104, -0.00974,
                0.00117, 0.03256, -0.02864, -0.02207, -0.05724, 0.04867, -0.01540, 0.00070,
                -0.01020, 0.02750, 0.06676, -0.00680, -0.19307, 0.01704, 0.01973, 0.02188,
                -0.00856, 0.03915, 0.03231, 0.07537, 0.07390, 0.10021, 0.29377, 0.13725 ]

dU      = catalog.data['dU']
dB      = catalog.data['dB']
dV      = catalog.data['dV']
dR      = catalog.data['dR']
dI      = catalog.data['dI']
dZn     = catalog.data['dZn']
dYhsc   = catalog.data['dYhsc']
dY      = catalog.data['dY']
dJ      = catalog.data['dJ']
dH      = catalog.data['dH']
dK      = catalog.data['dK']
dHw     = catalog.data['dHw']
dKw     = catalog.data['dKw']
dIA427  = catalog.data['dIA427']
dIA464  = catalog.data['dIA464']
dIA484  = catalog.data['dIA484']
dIA505  = catalog.data['dIA505']
dIA527  = catalog.data['dIA527']
dIA574  = catalog.data['dIA574']
dIA624  = catalog.data['dIA624']
dIA679  = catalog.data['dIA679']
dIA709  = catalog.data['dIA709']
dIA738  = catalog.data['dIA738']
dIA767  = catalog.data['dIA767']
dIA827  = catalog.data['dIA827']
dNB711  = catalog.data['dNB711']
dNB816  = catalog.data['dNB816']
dch1    = catalog.data['dch1']
dch2    = catalog.data['dch2']
dch3    = catalog.data['dch3']
dch4    = catalog.data['dch4']
dNUV    = catalog.data['dNUV']

magnitude_errors = [dU, dB, dV, dR, dI, dZn, dYhsc, dY, dJ, dH, dK, dHw, dKw, dIA427, dIA464,
                    dIA484, dIA505, dIA527, dIA574, dIA624, dIA679, dIA709, dIA738, dIA767,
                    dIA827, dNB711, dNB816, dch1, dch2, dch3, dch4, dNUV]

dflux = []

fluxes = []

for filt in magnitudes:
    fluxes.append(np.power(10, -0.4 * filt + 9.56))

for i in range(len(magnitudes)):
    dflux.append(np.abs( np.power(10, -0.4 * np.abs((magnitudes[i] - magnitude_errors[i])) + 9.56) - fluxes[i]) )

for i, id in enumerate(ids):
    if mask[i] == 0 and ra[i] < 150.827214 and ra[i] > 149.2842449215 and dec[i] > 1.498542 and dec[i] < 2.912468 and redshift[i] > 0.0 and redshift[i] < 6.0 and I_corr[i]> -90 and logmass[i] > 0 and nbands[i] > 2:  # ignore things that have been masked out

        line_output = str(id) + " " + str(logmass[i]) + " " + str(nbands[i]) + " " + str(ra[i]) + " " + str(dec[i]) + " " + str(I_corr[i]) + " " + str(redshift[i])
        flux_list = [str(f[i]) if f[i] < 1e30 else "0.0" for f in fluxes]
        error_list = [str(df[i]) for df in dflux]
        line_output += " " + " ".join(flux_list) + " " + " ".join(error_list) + "\n"
        sys.stdout.write(line_output)


#! /bin/bash

# Creates images for each channel

source ./SPHEREx.params
source utils.sh

errlog "\nGenerating simulated images in each band."

for channel in $Channels; do

    BandNoise="ch-${channel}-R${ChannelResolution[$(($channel - 1))]}.noise.txt"

    make_full_simim.pl $BandNoise && errlog "Finished generating images for channel $channel\n" &

done

wait 

errlog "Image generation is complete!"

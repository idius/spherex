#! /bin/bash

# Get the noise model information from the simap_util.py script, provided by 
# the engineering team. Gawk is used to remove output lines that do not consist of
# only whitespace and four columns of numbers. The regular expression below simply
# selects zero or more spaces at the beginning of the line, followed by three floating
# point numbers separated by whitespace, followed by a single digit and possibly some 
# more whitespace. NOTE: \s in gawk is short for [[:space:]] in standard awk.

source ./SPHEREx.params

source utils.sh

NoiseFile="noisefile"

errlog "Getting noise model from simap_util.py"

get_model_sense.py $MissionModelName | gawk '/^\s*([\.0-9]+\s+){3}[0-9]\s*$/ {print $0}' > $NoiseFile

# A List of all SPHEREx filters for simulated data
ModelList=""

# A list of all SPERHEx filters for the model grid and fitting
FittingList=""

# NoiseList will contain list of files containing parts of the noise model
NoiseList=""

# The noisefile contains channels. The next step loops over channels to create 
# filters: It takes a filter profile, shifts to right wavelength, then writes 
# out the filter

for channel in $Channels; do

    errlog "Creating simulated LVF filters for channel $channel"

    TempChannelNoise="channel-$channel.noise.cat"

    # convert the noise files to lc (imcat) format

    cat $NoiseFile | lc -C -n l -n rms -n Np -n ch \
                   | lc -i "%ch $channel ==" l "rms = %rms %Np sqrt /" \
                   > $TempChannelNoise

    Idx=$(($channel - 1))

    # make the LVF filter profiles (in reality, these are continuous)

    make_LVF.pl ${ChannelResolution[$Idx]} ${ChannelMinWavelength[$Idx]} \
                ${ChannelMaxWavelength[$Idx]} ch-$channel 1 $TempChannelNoise \
                ${SubtractionError[$Idx]}

    # remove temporary file:

    rm $TempChannelNoise

    # add filenames to lists

    CurrentFilter="$FilterDir/ch-$channel-R${ChannelResolution[$Idx]}.list"

    ModelList="$ModelList $CurrentFilter"
    FittingList="$FittingList $CurrentFilter"
    NoiseList="$NoiseList ch-$channel-R${ChannelResolution[$Idx]}.noise.txt"

done

cat $NormalizationFilterList $ExternalDataFilterList $ModelList > $SimulatedDataFilterList
cat $ExternalDataFilterList $FittingList > $FittingDataFilterList
cat $NoiseList > $NoiseModel

errlog "Finished generating the new noise model.\nResults are stored in \
$NoiseModel, $SimulatedDataFilterList, $FittingDataFilterList, and files \
listed therein.\n"

# remove temporary files

#rm $NoiseFile
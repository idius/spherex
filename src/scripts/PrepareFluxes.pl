#!/usr/bin/perl

$usage = "$0 filter_file simfile noisefile\n";

$filter_file = shift(@ARGV) || die $usage;
$simfile = shift(@ARGV) || die $usage;
$noisefile = shift(@ARGV) || die $usage;

$Fmax = 8; # TODO: where does this come from?

#number of external filters from external database; N ext Filt
$n_external_filters = 8;

@filters=();

open(FILNAME,"< $filter_file");

$ff=0;
@CPIPES=();
$idx=0;

while(<FILNAME>)
{
  chomp;
  ($name,$file)=split;

  if($ff > $Fmax)
  {
    $fname = $file;
    $fname =~ s/.txt//;
    push(@filters, $fname);
    push(@CPIPES, $fname);
    system("mkdir -p $ENV{'SimulationCatalogDir'}");
    $PIPE = $fname;
    $fout = "$ENV{'SimulationCatalogDir'}/" . $fname . ".txt";
    open($PIPE," > $fout");
    $pp++;
    $idx++;
  }
  $ff++;
}

close(FILNAME);

$meanerr = 0;
open(NOISE, "< $noisefile");
@noiselevel = ();
$idx = 0;

while(<NOISE>)
{
  chomp;
  ($filter,$CHname,$R,$l,$dl,$noise,$PSFsigma)=split;
  
  $meanerr += 1.0 / ($noise * $noise);

  push(@noiselevel, $noise);

  $idx++;
}

close(NOISE);

$meanerr = 1.0 / sqrt($meanerr);

open(NLOG," | lc -C -n ID -n RA -n DEC -n mag -n z -n mod -n ebv -n rlaw -n nfact -n flux0 -n meanflux -n SNR -n chisq | lc -b > renorm.log");

printf STDERR "Reading integrated flux data.\n";

open(SIM,"< $simfile ");

while(<SIM>)
{
  chomp;
  @data=split;

  $ID = shift(@data);
  $RA = shift(@data);
  $DEC = shift(@data);
  $mag = shift(@data);
  $z = shift(@data);
  $mod = shift(@data);
  $ebv = shift(@data);
  $rlaw = shift(@data);
  $scale_factor = shift(@data);
  $ID = int($ID);

  $meanflux=0;
  $SNR=0;
  $chisq=0;
 
  for($i = $Fmax; $i < $#data; $i++)
  {
      $OUTFILE = $filters[$i - $Fmax];

	    printf $OUTFILE "%d %lf %lf %lf %le \n",$ID,$RA,$DEC,$z,$data[$i + 1];

	    if($i > ($Fmax + $n_external_filters))
      {
	        $meanflux += $data[$i + 1];

          $chi = $data[$i + 1] / $noiselevel[$i - $Fmax - $n_external_filters];
	   
	        $chisq += $chi * $chi;
	    }
  }

  $meanflux /= ($#data - $Fmax);
  $SNR = $meanflux / $meanerr;     

  printf NLOG "%d %lf %lf %lf %lf %d %lf %d %le %le %le %le %le\n",$ID,$RA,$DEC,$mag,$z,$mod,$ebv,$rlaw,$fact,$data[1],$meanflux,$SNR,$chisq;
}

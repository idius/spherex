#! /bin/bash

source ./SPHEREx.params
source utils.sh

errlog "\nMeasuring photometry from the simulated images and catalog of known positions."

errlog "Generating PSFs"

idl -e "make_psfs, \"$NoiseModel\", \"$PSFfits\"" > /dev/null 2> /dev/null
# Note: with GDL, there is a double free that happens here after the program has finished running.

errlog "(Don't worry if you saw an error message about a double free above.)"

errlog "Measuring photometry."

output_photometry_fits="${MissionName}_${MissionModelName}_photometry.fits"

idl -e "get_simap_optimalphot, \"$PositionCatalog\", \"$PSFfits\", \"$NoiseModel\", \"$MissionName\", 1.0, \"$output_photometry_fits\"" > /dev/null 2> /dev/null
# Note: with GDL, there is a double free that happens here after the program has finished running.

errlog "(Don't worry if you saw an error message about a double free above.)"

errlog "saving raw photometry data in $output_photometry_fits."

errlog "Generating SEDs from the measured photometry."

idl -e "make_phot_file_for_fitting, \"$output_photometry_fits\", \"$PhotometryData\", \"$OutputSedCatalog\"" > /dev/null 2> /dev/null

errlog "The measured SEDs have been saved in $OutputSedCatalog."

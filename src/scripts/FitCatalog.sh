#! /bin/bash

# A script for automatically fitting a template library to photometry in the COSMOS catalog. THIS IS NOT FINISHED!

source SPHEREx.params.template

if [ $# != 4 ]; then
    echo Usage: $0 catalog filter_list template_list output_catalog
    exit 3
fi

catalog=$1
filter_list=$2
template_list=$3
output_cat=$4

if [ ! -d "$FilterDir" ]; then
    echo "Before you start, you need to put your filter transmission files into $FilterDir/. Your filter list must be in this directory."
    exit 1  
fi

if [ ! -d "$TemplateDir" ]; then
    echo "Before you start, you need to put your filter transmission files into $TemplateDir/. Your template list must be in this directory."
    exit 2
fi

DFS=/dfs/fitcat

mkdir -p $DFS

cp -r $FilterDir

cp -r $TemplateDir

sorted_catalog=${catalog}.sorted

# sort the catalog by redshift

sort-by-column.py 7 $catalog $sorted_catalog

# split the catalog into sections that each contain a small number of unique redshift values

paritition_catalog.py $sorted_catalog

cp $sorted_catalog-part-* $DFS/

# make sure that the files are really written to disk

ansible all -a "sync"
sleep 5

# generate model grids for each of the partitions, generated in the previous step

# fitcat mkgrid <catalog> <filter_list> <template_list> <output file>

stratos-run "fitcat mkgrid" "%c% $DFS/$filter_list $DFS/$template_list %c%.grid" "$DFS/$sorted_catalog-part-*"

# fit the templates to the photometry, using the model grids

# fitcat fit <catalog> <grid> <final output file>

stratos-run "fitcat fit" "%c% %c%.grid $DFS/$template_list %c%.out" "$DFS/$sorted_catalog-part-*"

# combine the output files into one file

cat $DFS/*.out > $output_catalog.raw

# sort the output file by ID.

sort-by-column.py 1 $output_catalog.raw $output_catalog

echo "This script is not complete. Refer to documentation to perform a photometry fit."

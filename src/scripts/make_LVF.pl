#!/usr/bin/perl
use PDL;
use PDL::GSL::INTERP;

$usage = "$0 R lmin lmax fset_name Start_Fno Noise_model BKGnoise\n";

$pixSize="$ENV{'PixSizeInArcsec'}";
$pixPhysSize="$ENV{'PixSizeInMicrons'}";
$TelDiam="$ENV{'TelescopeDiameterInCm'}";
$OptAber="$ENV{'OpticalAberrationInMicrons'}";
$PTerr="$ENV{'PointingErrorInArcsec'}";

$R = shift(@ARGV) || die $usage;

$lmin = shift(@ARGV) || die $usage;
$lmax = shift(@ARGV) || die $usage;
$fset = shift(@ARGV) || die $usage;
$start = shift(@ARGV) || die $usage;
$noiseModel = shift(@ARGV) || die $usage;
$BKGnoise = shift(@ARGV) || die $usage;

$prefix = "$ENV{'FilterDir'}/R" . $R . "-" . $fset . "-";
$flistname = "$ENV{'FilterDir'}/" . $fset . "-R" . $R . ".list";

#read in the noise model
$noiseout = $fset . "-R" . $R . ".noise.txt";

$Nn=0;
open(NIN,"lc -o l rms < $noiseModel |");
while(<NIN>){
  chomp;
  ($l,$n)=split;
  push(@noiseLam,$l);
  push(@noiseRMS,$n);
  $Nn++;
}
close(NIN);
$Nn=-1;

#($noiseLam,$noiseRMS)=rcols($noiseModel);
#$noise_spl = PDL::GSL::INTERP->init('linear',$noiseLam,$noiseRMS);

open(FFILE,"> $flistname");
open(NOISE,"> $noiseout");

$l = $lmin;

$FF=$start;
while(($l+$dl/2.0)<$lmax){

  $name = $prefix . $FF . ".txt";

  $dl = $l/($R-0.5);
  $llcen =  ($l+($dl/2.0));

  #get the noise

  if($llcen<$noiseLam[0]){
    $noise = ($noiseRMS[0]-$noiseRMS[1])*($llcen-$noiseLam[0])/($noiseLam[0]-$noiseLam[1])+$noiseRMS[0];
  }elsif($llcen>$noiseLam[$Nn]){
    #printf STDERR "%lf %lf\n",$llcen,$noiseLam[$Nn-1];
    $noise = ($noiseRMS[$Nn-1]-$noiseRMS[$Nn])*($llcen-$noiseLam[$Nn-1])/($noiseLam[$Nn-1]-$noiseLam[$Nn])+$noiseRMS[$Nn];
  }else{

    $Lidx=0;
    while($llcen>$noiseLam[$Lidx]){
      $Lidx++;
    }
    
    $noise = ($noiseRMS[$Lidx-1]-$noiseRMS[$Lidx])*($llcen-$noiseLam[$Lidx-1])/($noiseLam[$Lidx-1]-$noiseLam[$Lidx])+$noiseRMS[$Lidx-1];
  }

  #figure out the optics

  $fnoise = sqrt($noise*$noise + $BKGnoise*$BKGnoise);

  $dmeter=$TelDiam/100.0;

  $diffsm = 1.22*$llcen*1e-6*180*3600/($dmeter*3.14159*$pixSize);
  $diffsm /=(2*sqrt(2*log(2)));

  $optsm = $OptAber/$pixPhysSize; #optical distortion
  $optsm /=(2*sqrt(2*log(2)));


  $ptnsm = $PTerr/$pixSize;
  $ptnsm /=(2*sqrt(2*log(2)));

  #printf STDERR "%lf %lf %lf\n",$diffsm,$optsm,$ptnsm;

  $totsm = sqrt($optsm*$optsm + $diffsm*$diffsm + $ptnsm*$ptnsm);
  $totsm *=$pixSize;

  system("lc 'l = %l $dl * 10000.0 * $llcen 10000.0 * +' t < $ENV{'LVF_Template'} | lc -o > $name");

  printf NOISE "F%d %s %d %lf %lf %lf %lf \n",$FF,$fset,$R,($l+($dl/2.0))*10000.0,$dl*10000.0,$fnoise,$totsm;
  printf FFILE "%s-F%d R%d-%s-%d.txt\n",$fset,$FF,$R,$fset,$FF;

  $l+=$dl;
  $FF++;

}

close(FFILE);
close(NOISE);


#!/usr/bin/perl

$usage = "$0 input_cat\n";

$inputExtcat = shift(@ARGV) || die $usage;

print STDERR "FDATA is $ENV{'ExternalDataFilterList'}\n";

open(FDATA," < $ENV{'ExternalDataFilterList'}");

@depth=eval("$ENV{'PerlDepths'}");

while(<FDATA>){
  chomp;
  ($fname,$ffile)=split;
 
  $photlist .= " " . $fname;
  $Nf++;
}
close(FDATA);

for($i=0;$i<=$#depth;$i++){
  $err[$i]=(10**(($depth[$i]-23.9)/-2.5))/5;

  printf STDERR " depth = %lf, error = %lf \n",$depth[$i],$err[$i];
}

open(INPUT, "lc -o ID RA DEC redshift $photlist < $inputExtcat |");

while(<INPUT>){
  chomp;
  @data=split;
  
  $ID = shift(@data);
  $RA = shift(@data);
  $DEC = shift(@data);
  $redshift = shift(@data);
  $ID = int($ID);

  printf STDOUT "%d %lf %lf %lf ",$ID,$RA,$DEC,$redshift;
  
  $ff=0;
  foreach $item (@data){
    
    $perr = 0;
    $berr = $err[$ff]*grand();

    $flux = $item + $perr + $berr;
    if($flux > 0){
      $objerr = $err[$ff];
    }else{
      $objerr = $err[$ff];
    }

    printf STDOUT "%le %le ",$flux,$objerr;

    $ff++;
  }
  printf STDOUT "\n";
}

sub grand{
    my $v1,$v2,$r;
    if( defined $v2 ){
        $v1 = $v2;
        undef $v2;
    }else{
        do{
            $v1=rand(2)-1;
            $v2=rand(2)-1;
            $r = $v1*$v1+$v2*$v2;
        }while( $r >= 1 || $r == 0 );
        $r = sqrt(-2*(log $r)/$r);
        $v1 *= $r;
        $v2 *= $r;
   }
   return $v1;
}

import numpy as np
import pylab as pl
import math, sys
from scipy import constants as cst

def read_RdP_FM_ini(filename):

    #filename = '../Data/FMparams/params.EUCLID-kmax0.2-527'
    #filename = '../Data/FMparams/params.v11_base_req_nbandsgt5'
    file = open(filename,'r')
    surv = {} 
    for line in file:
        line.strip()
        word = line.split()
        if len(word)>0:
            if word[0] != '#':
                if word[0] == 'npop':
                    surv['Npop'] = int(word[2])
                if word[0] == 'Nzbin':
                    surv['Nzbin'] = int(word[2])
                if word[0] == 'sigz0':
                    surv['sigz0'] = np.array(map(float,word[2:]))
                if word[0] == 'zPk':
                    surv['zPk'] = np.array(map(float,word[2:]))
    if 'Npop' not in surv.keys():
        surv['Npop']=1# Set default, eg for WFIRST
    bias = np.zeros((surv['Npop'],surv['Nzbin']))
    dens = np.zeros((surv['Npop'],surv['Nzbin']))
    # Not optimal way to do it, but oh well...
    file.seek(0)
    for line in file:
        line.strip()
        word = line.split()
        if len(word)>0:
            if word[0] != '#':
                # This assume Nzbin and Npop are defined previously
                for ip in range(1,surv['Npop']+1):
                    str = 'numdens{0:1d}'.format(ip)
                    if word[0] == str:
                        dens[ip-1,:] = map(float,word[2:])
                    str = 'galaxy_bias{0:1d}'.format(ip)
                    if word[0] == str:
                        bias[ip-1,:] = (map(float,word[2:]))
    surv['bias'] = bias
    surv['dens'] = dens
    #print surv
    file.close()
    return surv
'''
#sigz = sigz0 * (1 + z)
sigz0 = 0.003 0.01 0.03 0.1

#number of galaxy populations
npop = 4

#number of redshift bins
Nzbin = 8

#number density [(h^-1 Mpc)^-3] population 1 (need Nzbin entries)
numdens1 = 0.00648 0.00172 6.02e-05 3.15e-06 1e-09 1e-09 1e-09 1e-09
numdens2 = 0.0075 0.00465 0.00085 0.000143 2.59e-05 6.06e-07 1e-09 1e-09
numdens3 = 0.00626 0.00581 0.00248 0.00134 0.000567 2.12e-05 5.14e-07 1e-09
numdens4 = 0.0104 0.0075 0.003 0.0024 0.00156 0.000111 8.22e-06 1.65e-06


#edges of redshift bins (need Nzbin+1 entries)
zPk = 0.0 0.2 0.4 0.6 0.8 1.0 1.6 2.2 2.8

#galbias population 1 (need Nzbin entries)
galaxy_bias1 = 0.82 1.1 2.3 4.3 1.0 1.0 1.0 1.0
galaxy_bias2 = 0.76 0.88 1.3 2.1 3.1 6.3 1.0 1.0
galaxy_bias3 = 0.74 0.82 1.0 1.3 1.7 3.7 7.2 1.0
galaxy_bias4 = 0.72 0.78 0.95 1.1 1.3 2.7 5.0 6.9
'''


def muJytoMab(muJy):
    Mab  = -2.5*np.log10(1.e-29*muJy) - 48.6
    return Mab

def MabtomuJy(Mab):
    muJy = 10.**(-(Mab+48.6)/2.5)*1.e29
    return muJy

def define_surveys():

    ## Define other surveys
    ## ====================

    SDSS = {'Name':'SDSS','Area':9000.,'Depth':[('U',22.0),('g',23.),('r',22.2),('i',21.8),('z',20.3)], \
            'lmin':3800,'lmax':9200,'R':1800}
    # Fig 7
    #http://iopscience.iop.org/article/10.1088/0067-0049/213/1/12/pdf;jsessionid=3C234319A837FBB99DA43E554C79F594.c4.iopscience.cld.iop.org
    #cor = -2.5*np.log10(5./2.)
    #SDSS = {'Name':'SDSS','Area':9000.,'Depth':[('U',22.0+cor),('g',22.2+cor),('r',22.2+cor),('i',21.3+cor),('z',20.+cor)], \
    #        'lmin':3800,'lmax':9200,'R':1800}
    # Extra-term to correct from 2 sig to 5 sig
    # Probably not correct for SDSS  http://www.sdss3.org/dr10/scope.php .
    #
    BOSS = {'Name':'BOSS','Area':10000.,'ngal':684000.,'zmin':np.array([0.1]),\
            'zmax':np.array([0.9]),'lmin':0.38,'lmax':0.92,'R':1800,\
            'label':'Baryonic Oscillation Spectroscopic Survey (BOSS)'}
    # R vary from 1500 to 2500
    #\
    WISE = {'Name':'WISE','Area':40000.,'Depth':[('W1',muJytoMab(0.08e3)),('W2',muJytoMab(0.11e3)),\
            ('W3',muJytoMab(1.e3)),('W4',muJytoMab(6.e3))],'R':1.2/3.4}
    #
    # In regions that are not confusion limited, AllWISE achieves SNR=5 flux at 54, 71, 730 and 5000 mJy (16.9, 16.0, 11.5 and 8.0 mag) in
    # W1, W2, W3 and W4, respectively. The W1 and W2 sensitivity is improved relative to that in the All-Sky Release Catalog.
    # The W3 and W4 sensitivity is also slightly improved relative to that in the All-Sky Catalog because of the Single-exposure
    # recalibration and improved local background estimation algorithms used for AllWISE data processing. 
    #http://wise2.ipac.caltech.edu/docs/release/allwise/expsup/sec2_1.html
    cor = -2.5*np.log10(5./2.)
    AllWISE = {'Name':'AllWISE','Area':40000.,'Depth':[('W1',19.1+0.3),('W2',18.8+0.5),\
                                                       ('W3',11.5),('W4',8.)],'R':1.2/3.4}
    #
    HSC = {'Name':'HSC','Area':1500.,'Depth':[('g',26.3),('r',26.0),('i',26.0),('z',24.9),\
                                              ('y',23.8)],'R':0.138/0.52}
    #
    cor = -2.5*np.log10(5./10.)
    DES = {'Name':'DES','Area':5000.,'Depth':[('g',25.2+cor),('r',24.8+cor),('i',24.0+cor),\
                                              ('z',23.4+cor),('y',21.7+cor)],'R':0.138/0.52}
    # Correct from 10 sigma to 5 sigma
    #
    fac = -2.5*np.log10(5./10.)
    TWOMASS = {'Name':'2MASS','Area':40000.,'Depth':[('J',15.8+fac),('H',15.1+fac),('K',14.3+fac)],'R':0.307/1.63}
    # fac converts from 10 sig to 5 sig
    #
    cor = -2.5*np.log10(5./10.)
    EuclidP = {'Name':'Euclid Photometric','Area':15000.,'Depth':[('VIS',24.5+cor),('Y',24),('J',24),('H',24)]}
    ## The numbers are 5 sig only VIS is 10 sig, hence the correction. From Peter Capak's email on 10/8/14
    EuclidS = {'Name':'Euclid Spectroscopic','Area':15000.,
               'Depth':[('I',19.6)],'R':250.,'lmin':1.1,'lmax':2.0,'ngal':52.e6,'zmin':1.2,'zmax':2.2,\
               'nz':14,'zmin':np.array([1.05,1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95]),\
               'zmax':np.array([1.15,1.25,1.35,1.45,1.55,1.65,1.75,1.85,1.95,2.05]),\
               'npop':1,'nden':np.array([1.68e-3,1.51e-3,1.35e-3,1.20e-3,1.0e-3,0.8e-3,0.58e-3,0.38e-3,0.35e-3,0.21e-3,0.11e-3]),\
                'bias':np.array([1.4,1.5,1.5,1.5,1.6,1.6,1.6,1.7,1.7,1.7]),\
                'label':'Euclid Spectroscopic'}
    #
    PFS     = {'Name':'PFS','Area':1400.,'Depth':[],'ngal':4.e6,'zmin':np.array([0.6]),'zmax':np.array([2.4]),
               'lmin':np.array([0.38,0.65,0.97]),\
               'lmax':np.array([670.,1000.,1260.]),'R':np.array([1900.,2400.,3500.]),\
               'label':'Subaru Prime Focus Spectrograph (PFS)'}
    #
    DESI    = {'Name':'DESI','Area':14000.,'ngal':24.e6,'zmin':0.2,'zmax':1.7,'lmin':0.36,'lmax':0.98,'R':np.array([1500.,3000.,4000.]),\
               'nz':9,'npop':3,'zmin':np.array([0.1,0.3,0.5,0.7,0.9,1.1,1.3,1.5,1.7]),'zmax':np.array([0.3,0.5,0.7,0.9,1.1,1.3,1.5,1.7,1.9]),\
                'nden':np.array([[0.000943,0.000328,0.000356,0.000505,0.000476,0.000407,0.000242,7.88e-05,7.38e-06],\
                                 # ELG
                                 [0.000283,0.000304,0.000304,0.000228,2.43e-05,1.93e-07,1e-12,1e-12,1e-12],\
                                 # LRG
                                 [4.88e-05,3.34e-05,1.99e-05,1.71e-05,1.76e-05,1.86e-05,1.99e-05,1.93e-05,1.34e-05]]),\
                                 # QSO
                'bias':np.array([[0.926,1.02,1.12,1.23,1.35,1.46,1.58,1.7,1.82],\
                                 [0.87,2.07,2.28,2.5,2.72,2.96,3.2,3.44,3.69],\
                                 [1.32,1.46,1.61,1.76,1.92,2.09,2.26,2.43,2.6]]),\
                'label':'Dark Energy Spectroscopic Instrument (DESI)'}
    #
    HETDEX  = {'Name':'HETDEX','Area':420.,'ngal':0.8e6,'zmin':np.array([1.9]),\
               'zmax':np.array([3.5]),'lmin':0.350,'lmax':0.550,'R':700,\
               'label':'Hobby-Eberly Telescope Dark Energy Experiment (HETDEX)'}
    #
    SPHEREx_P  = {'Name':'SPHEREx Target','Area':40000.,'ngal':312.e6,'lmin':0.5,'lmax':5.3,'R':40,\
                'nz':6,'npop':1,'zmin':np.array([0.0,0.2,0.4,0.6,0.8,1.,1.4,1.9,2.4,3.1,3.25,3.8]),\
                'zmax':np.array([0.2,0.4,0.6,0.8,1.,1.4,1.9,2.4,3.1,3.25,3.8,6.]),\
                'nden':np.array([2.18e-02,9.45e-03,4.00e-03,1.305e-03,5.0e-04,2.19e-05,5.55e-07,2.56e-07,3.40e-07,1.05e-07,0.,0.]),\
                'label':'SPHEREx Pessimistic'}
    #
    # PanSTARR
    #PanSTARR = {'Name':'Pan-STARRS1','Area':30000,'Depth':[('g',23.0),('r',22.8),('y',20.8),('i',22.5),('z',21.7)]}
    PanSTARR = {'Name':'Pan-STARRS1','Area':30000,'Depth':[('g',23.15),('r',22.95),('y',21.05),('i',22.75),('z',21.95)]}
    ## From Lihwai
    ##     5 sigma, 3" aperture in diameter)
    ## g   23.15
    ## r    23.95
    ## i    22.75
    ## z    21.95
    ## y    21.05
    # #Depth of external data in same list as ExtDataList
    #@Fnames=('G','R','I','Z','Y','WISE1','WISE2');
    #@depth=(23.0,22.8,22.5,21.7,20.8,19.1,18.8);
    #
    SPHEREx_O = {'Name':'SPHEREx','Area':40000.,'ngal':101.e6,'lmin':0.5,'lmax':5.3,'R':40,\
                'nz':6,'npop':1,'zmin':np.array([0.0,0.2,0.4,0.6,0.9,1.2]),'zmax':np.array([0.2,0.4,0.6,0.9,1.2,1.4]),\
                'nden':np.array([1.054e-02,3.579e-03,5.869e-04,1.043e-04,1.045e-05,0]),\
                'bias':np.array([0.904,1.05,1.21,1.43,1.73,2.04,2.41]),'label':'SPHEREx'}# (blue/ELG)
                #galaxy_bias2 = 1.83 2.12 2.44 2.9 3.5 4.12 4.87 (red/LRG)
    #
    CV       = {'Name':'Ideal','Area':40000.,'ngal':101.e10,'zmin':0.,'zmax':5.,'lmin':0.5,'lmax':5.3,'R':40000}
    #
    surveys_def = {'SDSS':SDSS,'WISE':WISE,'AllWISE':AllWISE,'HSC':HSC,'Euclid P':EuclidP,'Euclid S':EuclidS,'2MASS':TWOMASS,'PFS':PFS,\
                   'DES':DES,'SPHEREx_O':SPHEREx_O,'SPHEREx_P':SPHEREx_P,'BOSS':BOSS,'DESI':DESI,'HETDEX':HETDEX,'CV':CV,'PanSTARR':PanSTARR}

    #
    #HSC-Wide 26.3 26.0 26.0 24.9 23.8
    # DES 10 sigma in table 3
#   # https://www.darkenergysurvey.org/survey/des-description.pdf

    ## 2MASS
    #http://www.ipac.caltech.edu/2mass/releases/allsky/doc/sec1_6b.html#depth

    ## WISE
    # http://wise2.ipac.caltech.edu/docs/release/allsky/
    # WISE at 3.4, 4.6, 12, and 22 um (W1, W2, W3, W4)
    # an angular resolution of 6.1", 6.4", 6.5", & 12.0" in the four bands.
    # WISE achieved 5 sig point source sensitivities better than 0.08, 0.11, 1 and 6 mJy 

    ## BOSS
    # http://www.uv.es/alferso/surveys2012/RIAtalks/Mellier.pdf
                    #u	g	r	i	z
                    #3551 A	4686 A	6165 A	7481 A	8931 A
                    #22.0	22.2	22.2	21.3	20.5
                    #http://www.sdss3.org/dr9/scope.php (9274 sq.deg )
                    #Main sample galaxies	Petrosian r < 17.77	Strauss et al. (2002)
                    #Luminous Red Galaxies	Petrosian r < 19.2	Eisenstein et al. (2001)
                    #z < 3 quasars	PSF i < 19.1	Richards et al. (2002)
                    #z > 3 quasars	PSF i < 20.2
    #                 {'Name':'BOSS LRGs','Area':3275,'Depth':[('I',19.9)],'Range':[3600,10400],'R':1400},
    #{'Name':'BOSS QSOs','Area':3275,'Depth':[('G',22.0),('I',22)],'Range':[3600,10400],'R':1400}]
                    #http://www.sdss3.org/dr9/scope.php
                    #LRGs	i < 19.9	White et al. (2011) :: Spectro
                    #QSOs	g < 22 ; i < 22	
                    #               {'Name':'DES' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'LSST' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'PFS' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'Euclid (P)' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'Euclid (S)' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'WFIRST (P)' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'WFIRST (S)' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'JWST' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'WISE' ,'Area','Depth':[('U',23),('AB',)]},
                    #{'Name':'DESI' ,'Area','Depth':[('U',23),('AB',)]}]

    ## Euclid from http://www.uv.es/alferso/surveys2012/RIAtalks/Mellier.pdf
    ## (http://en.wikipedia.org/wiki/Photometric_system)

    return surveys_def

def define_astro_bands():

    # Bands are defined such that they extend from x-dl/2 to x+dl/2 (at least for WISE)...
    # FWHM x 2 for the others as in http://en.wikipedia.org/wiki/Photometric_system

    U = {'Band':'U','lam_eff':0.365,'dl':0.066}
    B = {'Band':'B','lam_eff':0.445,'dl':0.094}
    V = {'Band':'V','lam_eff':0.551,'dl':0.088}
    g = {'Band':'g','lam_eff':0.52,'dl':0.138} ## TBC
    R = {'Band':'R','lam_eff':0.658,'dl':0.138}
    r = {'Band':'r','lam_eff':0.658,'dl':0.138} # TBC
    VIS = {'Band':'VIS','lam_eff':0.725,'dl':0.175}
    I = {'Band':'I','lam_eff':0.806,'dl':0.149}
    i = {'Band':'i','lam_eff':0.806,'dl':0.149} # TBC
    z = {'Band':'z','lam_eff':0.900,'dl':0.13*0.91} # TBC
    Y = {'Band':'Y','lam_eff':1.020,'dl':0.13*0.91}
    y = {'Band':'y','lam_eff':1.020,'dl':0.13*0.91}
    J = {'Band':'J','lam_eff':1.22,'dl':0.213}
    H = {'Band':'H','lam_eff':1.63,'dl':0.307}
    K = {'Band':'K','lam_eff':2.19,'dl':0.390}
    L = {'Band':'L','lam_eff':3.450,'dl':0.13*3.450}
    M = {'Band':'M','lam_eff':4.750,'dl':0.460}
    W1 = {'Band':'W1','lam_eff':3.4,'dl':1.2}
    W2 = {'Band':'W2','lam_eff':4.6,'dl':1.2}
    W3 = {'Band':'W3','lam_eff':12.,'dl':10.5}
    W4 = {'Band':'W4','lam_eff':22.,'dl':7.5}

    # WISE at 3.4, 4.6, 12, and 22 um (W1, W2, W3, W4)

    bands_def ={'U':U,'B':B,'V':V,'g':g,'R':R,'r':r,'VIS':VIS,'I':I,'i':i,'z':z,'Y':Y,'y':y,'J':J,'H':H,\
                'K':K,'L':L,'M':M,'W1':W1,'W2':W2,'W3':W3,'W4':W4}
    return bands_def

def define_instrument(version,verbose=False):
    #='v8_baseline_cap'): ## No default version
    
    if (version=='v2'):
        Bands= {'lmin':(np.array([0.50,0.84,1.40,2.35]),'um'),\
                'lmax':(np.array([0.84,1.40,2.35,5.30]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([12.,12.,12.,12.]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.67,0.85,0.92,0.96]),'4xAI'),\
                'eta_mir_Au':(np.array([0.81,0.92,0.96,0.98]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([4.,4.,4.,4.]),''),\
                'nIn':(np.array([680.7,454.6,177.9,38.7]),'nW/m2/sr')}
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.675,'Tmission':(1.,'yr'),\
                'ShRed':(4.,'pyr'),'DpRed':(133.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(5.88,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'nbands':4,'Bands':Bands}

        if verbose:
            print 'Defined version ',version
            
    elif (version=='v4'):
        
        
        Bands= {'lmin':(np.array([0.90,1.42,2.23,2.80]),'um'),\
                'lmax':(np.array([1.42,2.23,3.50,4.80]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([12.,12.,12.,12.]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.74,0.88,0.94,0.97]),'4xAI'),\
                'eta_mir_Au':(np.array([0.86,0.94,0.97,0.99]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([3.44,3.63,4.13,4.76]),''),\
                'nIn':(np.array([650.1,284.3,100.0,55.0]),'nW/m2/sr')}
        
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.573,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(151.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(3.34,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'nbands':4,'Bands':Bands}

    elif (version=='v6'):
        
        '''
        Summary for current design in "20 cm baseline".  "20 cm v4" describes version 4.
        "20 cm threshold" describes first cut at threshold with R = 30
        1.  Adjusted band4 coverage to 2.6 - 5.0 um at R=150
        As a result of this change:
        A.  The number of Galactic plane pointings per year have increased from 14,609 to 15,862
        B.  The all-sky and Galactic plane integration time has gone from 100 s to 99 s
        C.  The average daily data volume changes very little
        D.  The band 4 point source sensitivity is very slightly worse
        '''
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([12.,12.,12.,12.]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.74,0.88,0.94,0.97]),'4xAI'),\
                'eta_mir_Au':(np.array([0.86,0.94,0.97,0.99]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([2.14,2.14,2.14,2.14]),''),\
                'nIn':(np.array([650.1,284.3,100.0,55.0]),'nW/m2/sr')}
        
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(134.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(3.62,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'nbands':4,'Bands':Bands,}


    elif (version=='v6_threshold'):
        
        '''
        Summary for current design in "20 cm baseline".  "20 cm v4" describes version 4.  "20 cm threshold" describes first cut at threshold with R = 30
        1.  Adjusted band4 coverage to 2.6 - 5.0 um at R=150
        As a result of this change:
        A.  The number of Galactic plane pointings per year have increased from 14,609 to 15,862
        B.  The all-sky and Galactic plane integration time has gone from 100 s to 99 s
        C.  The average daily data volume changes very little
        D.  The band 4 point source sensitivity is very slightly worse
        '''
                   
        Bands= {'lmin':(np.array([0.75,1.62]),'um'),\
                'lmax':(np.array([1.62,3.50]),'um'),\
                'R':(np.array([30.,30.]),''),\
                'Format':(np.array([2048.,2048.]),''),\
                'dQ':(np.array([12.,12.]),'e-'),\
                'Pitch':(np.array([18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.74,0.88]),'4xAI'),\
                'eta_mir_Au':(np.array([0.86,0.94]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5]),'s'),\
                'Nps':(np.array([3.3,3.3]),''),\
                 ## Corresponds to Npix_0 in the N_eff formula
                'nIn':(np.array([627.9,131.4]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':2.16,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(119.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(3.83,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.15,'um'),'nbands':2,'Bands':Bands}

    ## =============
    ## V7 CAPABILITY
    ## =============
    elif (version=='v7_capability'):
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.74,0.88,0.94,0.97]),'3xAI'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(134.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(2.92,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'Npix_0':(1.726875,'um'),'nbands':4,'Bands':Bands}

    ## =============
    ## V7 REQUIRED
    ## =============
    elif (version=='v7_required'):
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([18.0,18.0,15.0,15.0]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.74,0.88,0.94,0.97]),'3xAI'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.50,0.50,0.50,0.50]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([4.01,4.15,4.54,5.05]),''),\
                 ## Corresponds to Npix_0 in the N_eff formula
                 ## Irrelevant if Npix_0 is defined below
                'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(134.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(2.92,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'Npix_0':(3.9375,'um'),'nbands':4,'Bands':Bands}

    ## =============
    ## V7 Threshold
    ## =============
    ## Needs to add rms thing
    
    elif (version=='v7_threshold'):
                         
        Bands= {'lmin':(np.array([0.75,1.62]),'um'),\
                'lmax':(np.array([1.62,3.50]),'um'),\
                'R':(np.array([30.,30.]),''),\
                'Format':(np.array([2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.]),'um'),\
                'eta_mir_Ai':(np.array([0.74,0.88]),'4xAI'),\
                'eta_mir_Au':(np.array([0.94,0.97]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5]),'s'),\
                'Nps':(np.array([3.3,3.3]),''),\
                 ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                'nIn':(np.array([627.9,131.4]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':2.16,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(119.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'dQ(sh)':(3.83,'e-'),\
                'dQ(dp)':(2.65,'e-'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'Npix_0':(1.7268,'um'),'nbands':2,'Bands':Bands}

    ## ======================
    ## V8 BASELINE CAPABILITY
    ## ======================
    elif (version=='v8_baseline_cap'):
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(134.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':4,'Bands':Bands}

    ## ================
    ## V8 BASELINE RQMT
    ## ================
    elif (version=='v8_baseline_req'):
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([18.,18.,18.,18.]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.50,0.50,0.50,0.50]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(134.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.,'arcsec'),\
                'nbands':4,'Bands':Bands}


    ## ================
    ## V8 Threshold Cap
    ## ================
    ## Needs to add rms thing
    
    elif (version=='v8_thresv1_cap'):
                         
        Bands= {'lmin':(np.array([0.75,1.73]),'um'),\
                'lmax':(np.array([1.73,4.00]),'um'),\
                'R':(np.array([30.,30.]),''),\
                'Format':(np.array([2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.94,0.97]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5]),'s'),\
                'nIn':(np.array([627.9,131.4]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':2.31,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(109.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':2,'Bands':Bands}

    ## ================
    ## V8 Threshold Req
    ## ================
    ## Needs to add rms thing
                
    elif (version=='v8_thresv1_req'):
                         
        Bands= {'lmin':(np.array([0.75,1.73]),'um'),\
                'lmax':(np.array([1.73,4.00]),'um'),\
                'R':(np.array([30.,30.]),''),\
                'Format':(np.array([2048.,2048.]),''),\
                'dQ':(np.array([18.0,15.0]),'e-'),\
                'Pitch':(np.array([18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.88,0.88]),'4xAu'),\
                'eta_dich':(np.array([0.95,0.95]),''),\
                'eta_lvf':(np.array([0.50,0.50]),''),\
                'eta_fpa':(np.array([0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5]),'s'),\
                'nIn':(np.array([627.9,131.4]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':2.31,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(109.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.0,'arcsec'),\
                'nbands':2,'Bands':Bands}

    ## ================
    ## V8 Threshold Cap
    ## ================
    ## Needs to add rms thing
    
    elif (version=='v8_thresv2_cap'):                       
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([1448.,1448.,1448.,1448.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(67.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':4,'Bands':Bands}

    ## ================
    ## V8 Threshold Req
    ## ================
    ## Needs to add rms thing
    
    elif (version=='v8_thresv2_req'):                       
                   
        Bands= {'lmin':(np.array([0.75,1.25,2.09,2.60]),'um'),\
                'lmax':(np.array([1.25,2.09,3.50,5.00]),'um'),\
                'R':(np.array([40.,40.,40.,150.]),''),\
                'Format':(np.array([1448.,1448.,1448.,1448.]),''),\
                'dQ':(np.array([18.0,18.0,15.0,15.0]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.50,0.50,0.50,0.50]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')}
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.671,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'DpRed':(67.,'pyr'),\
                'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'lg/step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.0,'arcsec'),\
                'nbands':4,'Bands':Bands}

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V12 BASELINE CAP
    ## ======================
    elif (version=='v12_baseline_cap'):
                   
        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }
            
        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':4,'Bands':Bands,'no_gal_survey':True}

        if verbose:
            print 'Defined version ',version


    ## ======================
    ## V12 BASELINE REQ
    ## ======================
    elif (version=='v12_baseline_req'):
        
        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([15.0,15.0,15.0,15.0]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.50,0.50,0.50,0.50]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.0,'arcsec'),\
                'nbands':4,'Bands':Bands,'no_gal_survey':True}

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V12 TH CAP2
    ## ======================
    elif (version=='v12_thres_cap2'):

        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.75,0.75,0.75,0.75]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':4,'Bands':Bands,'array_size_factor':0.5,'no_gal_survey':True}

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V12  TH REQ2
    ## ======================
    elif (version=='v12_thres_req2'):

        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([15.0,15.0,15.0,15.0]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.50,0.50,0.50,0.50]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'dpAS':(2.,'p.orb.'),'lgAS':(4.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.0,'arcsec'),\
                'nbands':4,'Bands':Bands,'array_size_factor':0.5,'no_gal_survey':True}

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V13 BASELINE MEV (req)
    ## ======================
    elif (version=='v13_baseline_mev'):

        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'Smile_loss':(np.array([76.,76.,76.,76.]),'pix'),\
                'Pad_loss':(np.array([47.,47.,47.,47.]),'pix'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.80,0.80,0.80,0.80]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'ShIneff':(1.05,''),'DeepIneff':(1.25,''),\
                'dpAS':(1.6,'p.orb.'),'lgAS':(5.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'TM_dl':(59.,'p.orb.'),'SAA_time':(171.,'p.orb'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.,'arcsec'),\
                'nbands':4,'Bands':Bands,'no_gal_survey':True\
                }

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V13 BASELINE CBE (cap)
    ## ======================
    elif (version=='v13_baseline_cbe'):

        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'Smile_loss':(np.array([76.,76.,76.,76.]),'pix'),\
                'Pad_loss':(np.array([47.,47.,47.,47.]),'pix'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.85,0.85,0.85,0.85]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'ShIneff':(1.05,''),'DeepIneff':(1.25,''),\
                'dpAS':(1.6,'p.orb.'),'lgAS':(5.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'TM_dl':(59.,'p.orb.'),'SAA_time':(171.,'p.orb'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':4,'Bands':Bands,'no_gal_survey':True\
                }

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V14 BASELINE MEV
    ## ======================
    elif (version=='v14_baseline_mev'):

        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([15.0,15.0,15.0,15.0]),'e-'),\
                'DC':(np.array([0.05,0.05,0.05,0.05]),'e-/s'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'Smile_loss':(np.array([76.,76.,76.,76.]),'pix'),\
                'Pad_loss':(np.array([47.,47.,47.,47.]),'pix'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.80,0.80,0.80,0.80]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'ShIneff':(1.05,''),'DeepIneff':(1.25,''),\
                'dpAS':(1.6,'p.orb.'),'lgAS':(5.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'TM_dl':(59.,'p.orb.'),'SAA_time':(171.,'p.orb'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(3.,'arcsec'),\
                'nbands':4,'Bands':Bands,'no_gal_survey':True\
                }

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V14 BASELINE CBE
    ## ======================
    elif (version=='v14_baseline_cbe'):

        Bands= {'lmin':(np.array([0.75,1.32,2.34,4.12]),'um'),\
                'lmax':(np.array([1.32,2.34,4.12,4.83]),'um'),\
                'R':(np.array([41.5,41.5,41.5,150.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'DC':(np.array([0.01,0.01,0.01,0.01]),'e-/s'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'Smile_loss':(np.array([76.,76.,76.,76.]),'pix'),\
                'Pad_loss':(np.array([47.,47.,47.,47.]),'pix'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.85,0.85,0.85,0.85]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                ##'Nps':(np.array([1.7268,1.7268,1.7268,1.7268]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.765,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(200.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'ShIneff':(1.05,''),'DeepIneff':(1.25,''),\
                'dpAS':(1.6,'p.orb.'),'lgAS':(5.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'TM_dl':(59.,'p.orb.'),'SAA_time':(171.,'p.orb'),\
                'lam_pix':(3.6,'um'),'rms_pointing':(1.5,'arcsec'),\
                'nbands':4,'Bands':Bands,'no_gal_survey':True\
                }

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V16 BASELINE MEV
    ## ======================
    elif (version=='v16_baseline_mev'):

        Bands= {'lmin':(np.array([0.75,1.33,2.36,4.18]),'um'),\
                'lmax':(np.array([1.33,2.36,4.18,5.00]),'um'),\
                'R':(np.array([41.38,41.38,41.38,135.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([15.0,15.0,15.0,15.0]),'e-'),\
                'DC':(np.array([0.05,0.05,0.05,0.05]),'e-/s'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'Smile_loss':(np.array([76.,76.,76.,76.]),'pix'),\
                'Pad_loss':(np.array([47.,47.,47.,47.]),'pix'),\
                'eta_mir_Au':(np.array([0.88,0.88,0.88,0.88]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.80,0.75,0.75,0.65]),''),\
                'eta_fpa':(np.array([0.70,0.70,0.70,0.70]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([7.0,7.0,7.0,7.0]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.773,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(100.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'ShIneff':(1.15,''),'DeepIneff':(1.3,''),\
                'dpAS':(1.5,'p.orb.'),'lgAS':(6.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(150.,'s.'),\
                'smSS':(20.,'s'),'Torb':(95,'min'),\
                'TM_dl':(59.,'p.orb.'),'SAA_time':(171.,'p.orb'),\
                'lam_pix':(3.29511,'um'),'rms_pointing':(-1.,'arcsec'),\
            #Lam_pix table read from the array directly
            # Set rms_pointing to -1 so as to use direct fit
                'nbands':4,'Bands':Bands,'no_gal_survey':True\
                }

        if verbose:
            print 'Defined version ',version

    ## ======================
    ## V16 BASELINE CBE
    ## ======================
    elif (version=='v16_baseline_cbe'):

        Bands= {'lmin':(np.array([0.75,1.33,2.36,4.18]),'um'),\
                'lmax':(np.array([1.33,2.36,4.18,5.00]),'um'),\
                'R':(np.array([41.38,41.38,41.38,135.]),''),\
                'Format':(np.array([2048.,2048.,2048.,2048.]),''),\
                'dQ':(np.array([10.5,10.5,10.5,10.5]),'e-'),\
                'DC':(np.array([0.01,0.01,0.03,0.03]),'e-/s'),\
                'Pitch':(np.array([18.,18.,18.,18.]),'um'),\
                'Smile_loss':(np.array([76.,76.,76.,76.]),'pix'),\
                'Pad_loss':(np.array([47.,47.,47.,47.]),'pix'),\
                'eta_mir_Au':(np.array([0.94,0.97,0.97,0.97]),'3xAu'),\
                'eta_dich':(np.array([0.95,0.95,0.95,0.95]),''),\
                'eta_lvf':(np.array([0.90,0.80,0.80,0.70]),''),\
                'eta_fpa':(np.array([0.75,0.75,0.75,0.75]),''),\
                'T_samp':(np.array([1.5,1.5,1.5,1.5]),'s'),\
                'Nps':(np.array([3.25,3.25,3.25,3.25]),''),\
                ## Corresponds to Npix_0 in the N_eff formula
                ## Irrelevant if Npix_0 is defined below
                ##'nIn':(np.array([788.5,338.7,106.0,55.0]),'nW/m2/sr')
                }

        Inst = {'Aperture':(20.,'cm'),'f':(3.,''),\
                'lmax/lmin':1.773,'Tmission':(2.,'yr'),\
                'ShRed':(2.,'pyr'),'Adeep':(100.,'sq. deg'),'lch':(1.,'Nyq/2'),\
                'ShIneff':(1.15,''),'DeepIneff':(1.3,''),\
                'dpAS':(1.5,'p.orb.'),'lgAS':(6.,'p.orb.'),\
                'smdp':(4.,'/lg. step'),'lgSS':(90.,'s.'),\
                'smSS':(10.,'s'),'Torb':(95,'min'),\
                'TM_dl':(59.,'p.orb.'),'SAA_time':(171.,'p.orb'),\
                'lam_pix':(3.409971,'um'),'rms_pointing':(-1.,'arcsec'),\
            #Lam_pix table read from the array directly
            # Set rms_pointing to -1 so as to use direct fit
                'nbands':4,'Bands':Bands,'no_gal_survey':True\
                }

        if verbose:
            print 'Defined version ',version 
            
    else:
        print "Instrument version unknown in 'define_instr'",version
        quit()

    return(Inst)

def match_lambda_to_band(Inst,lam):
    # For a given lambda, return bands number or -1 if out of bands
    # Probably a better python way to do it... but it is a small array here.

    nbands = Inst['nbands'] # nband
    lmin   = Inst['Bands']['lmin'][0][:]
    lmax   = Inst['Bands']['lmax'][0][:]
    # Too low
    if lam < lmin[0]:
        ib = -1
        return ib
    # Too high
    if lam > lmax[nbands-1]:
        ib = -1
        return ib
    # In band...
    ib = 0
    while not (lam >= lmin[ib] and lam < lmax[ib]):
        ib += 1
    return ib-1

def compute_sensitivities(Inst,verbose=False):#,lambda,nIn_ppix,dF):

    ## Physical cst
    c0 = cst.value('speed of light in vacuum')*1.e6 #um/s
    h0 = cst.value('Planck constant') # J.s
    kb = cst.value('Boltzmann constant') # J/K
    hc = c0*h0 # um.J

    # Rename a few variable
    nbands= Inst['nbands'] # nband
    Pitch = Inst['Bands']['Pitch'][0][:] # um
    '''
    if ('Npix_0' in Inst): # Need for version => v7
        nps_0 = Inst['Npix_0'][0]
        nps_0 = np.repeat(nps_0,nbands)
    else:
        nps_0 = Inst['Bands']['Nps'][0][:]
    '''
    if ('rms_pointing' in Inst): # Need for version => v>=7
        nps_0 = 0.99+1.31*(Inst['rms_pointing'][0]/2.)**2
        nps_0 = np.repeat(nps_0,nbands)
    else:
        nps_0 = Inst['Bands']['Nps'][0][:]
    if (Inst['rms_pointing'][0] == -1): # Need for version => v>=16
        nps_0 = Inst['Bands']['Nps'][0][:]
        print 'In the right place...'
    Apert = Inst['Aperture'][0] # Aperturce in cm
    fnum  = Inst['f'][0] # fnumber
    R     = Inst['Bands']['R'][0][:]
    lch   = Inst['lch'][0]
    dp_AS_steps = Inst['dpAS'][0]
    dp_sm_steps = Inst['smdp'][0]
    lg_AS_steps = Inst['lgAS'][0]  #/orbit
    sm_dp_steps = Inst['smdp'][0]  #/orbit
    lmax  = Inst['Bands']['lmax'][0][:]
    lmin  = Inst['Bands']['lmin'][0][:]
    ShRed = Inst['ShRed'][0]
    Torb  = Inst['Torb'][0]
    Area_dp = Inst['Adeep'][0]
    Tmission = Inst['Tmission'][0]
    lam_pix  = Inst['lam_pix'][0] ## Fitted parameter for the N_eff scaling

    #print lam_pix
    ZL_fac = 1.5 # Zodi fudge factor for >v4

    # Derived Instrument Constant
    th_pix   = 3600.*180./np.pi*Pitch*1.e-4/(Apert*fnum) #arcsec. 
    #th_spec  = th_pix/60.*lch*2048./(R*np.log(lmax/lmin)) #arcmin.
    #if ('array_size_factor' in Inst): # Need for version => v>=11 threshold
    #    # Take into account the increase array size in the threshold cases of v11
    #    th_spec *= Inst['array_size_factor']
    #    if verbose:
    #        print '---'
    #        print 'Multiplied the spectral step by',Inst['array_size_factor'],' to account for array size'
    #        print '---'
    #print th_spec
    steps    = np.ceil(R*np.log(lmax/lmin)/lch)  ## Round up to 24
    th_im_x  = th_pix/60.*Inst['Bands']['Format'][0][:]/steps # arcmin. (spec theta in spreadsheet)
    if (('Smile_loss' in Inst['Bands']) and ('Pad_loss' in Inst['Bands'])):
        th_im_x = th_pix/60.*(Inst['Bands']['Format'][0][:]-\
            Inst['Bands']['Smile_loss'][0][:]-\
            Inst['Bands']['Pad_loss'][0][:])/steps # arcmin.
        #print 'Tt ',Inst['Bands']['Format'][0][:],\
        #  Inst['Bands']['Smile_loss'][0][:],Inst['Bands']['Pad_loss'][0][:]
    th_im_y  = Inst['Bands']['Format'][0][:]*th_pix/3600. # Angular size along the y direction [Deg.]
    spec_fov = th_im_x/60.*th_im_y # deg^2
    #print 'Spec ',spec_fov,th_im_y,th_im_x
    #assert(0)
    num_orbits   = 365.25*24.*60./Inst['Torb'][0] # /yr
    th_im        = th_im_y*th_im_x/60. # Deg^2
    num_step_gal = 0.# (360./th_im)*(steps[nbands-1]-steps[0])*ShRed# /yr ## Set to 0 after v12.
    if ('no_gal_survey' in Inst):
        if (Inst['no_gal_survey']):
            num_step_gal = 0.
    Red_dp       = num_orbits*dp_AS_steps*dp_sm_steps/(Area_dp/spec_fov) # deep survey redundancy (/yr)
    #if ('array_size_factor' in Inst): # Need for version => v>=11 threshold
    #    Red_dp   = num_orbits*dp_AS_steps*dp_sm_steps/((2.*Area_dp/th_im**2)*steps) # deep survey redundancy
    
    #num_step_fs_sh = 4.*np.pi*(180./np.pi)**2./th_im**2*ShRed*steps# /yr
    #print th_spec
    #print th_im_x
    #print th_im_y
    #print spec_fov 
    num_step_fs_sh = 4.*np.pi*(180./np.pi)**2./spec_fov*ShRed # /yr
    if ('ShIneff' in Inst): # Need for version => v>=13 (Survey Inefficiency)
        num_step_fs_sh *= Inst['ShIneff'][0]
    #num_step_dp    = Red_dp*steps*Area_dp/th_im**2
    num_step_dp    = num_orbits*dp_AS_steps*dp_sm_steps
    #if ('DeepIneff' in Inst): # Need for version => v>=13 (Survey Inefficiency)
    #    num_step_dp *= Inst['DeepIneff'][0]
    #if ('array_size_factor' in Inst): # Need for version => v>=11 threshold
    #    # Take into account the increase array size in the threshold cases of v11
    #    num_step_fs_sh /= Inst['array_size_factor']
    #    num_step_dp    /= Inst['array_size_factor']
    sm_AS_steps = num_step_fs_sh/num_orbits/lg_AS_steps
    num_sm_step_sh_po = ((num_step_gal+num_step_fs_sh)-num_orbits*lg_AS_steps)/num_orbits
    num_sm_step_dp_po = (num_step_dp-num_orbits*dp_AS_steps)/num_orbits
    print Inst['TM_dl'][0]
    f_sh        = lg_AS_steps/(lg_AS_steps+dp_AS_steps)
    t_obs_sh    = Torb*60.-(lg_AS_steps+dp_AS_steps)*Inst['lgSS'][0]\
                - lg_AS_steps*(sm_AS_steps-1.)*Inst['smSS'][0]\
                - dp_AS_steps*(sm_dp_steps-1.)*Inst['smSS'][0]\
                - Inst['TM_dl'][0]-Inst['SAA_time'][0]
    t_obs_sh   /= (lg_AS_steps+dp_AS_steps)*sm_AS_steps
    #                ((num_step_gal+num_step_fs_sh)/num_orbits-4.)*Inst['smSS'][0]
    t_obs_dp    = Torb*60.-(lg_AS_steps+dp_AS_steps)*Inst['lgSS'][0]\
                - lg_AS_steps*(sm_AS_steps-1.)*Inst['smSS'][0]\
                - dp_AS_steps*(sm_dp_steps-1.)*Inst['smSS'][0]\
                - Inst['TM_dl'][0]-Inst['SAA_time'][0]
    t_obs_dp   /= (lg_AS_steps+dp_AS_steps)*sm_dp_steps
    #t_obs_dp    = Torb*60.*(1.0-f_sh)-dp_AS_steps*Inst['lgSS'][0]-\
    #            (num_step_dp/num_orbits-4.)*Inst['smSS'][0]
    #t_int_sh    = t_obs_sh/(lg_AS_steps+num_sm_step_sh_po)
    t_int_sh    = t_obs_sh[1]
    # np.max(t_int_sh) # Chooses band 2 to define shallow Integration time (the smaller of band 1-3)
    # We define the integration time as the longer one required for shallow survey full sampling.
    #
    #t_int_dp    = t_obs_dp/(dp_AS_steps+num_sm_step_dp_po)
    t_int_dp    = np.min(t_obs_dp)#np.max(t_int_dp)
    #print num_orbits
    #print t_int_sh, t_int_dp
    #assert(0)
    #
    eta_opt    = Inst['Bands']['eta_mir_Au'][0][:]*Inst['Bands']['eta_dich'][0][:]\
        *Inst['Bands']['eta_lvf'][0][:]
    eta_tot    = eta_opt*Inst['Bands']['eta_fpa'][0][:]
    AOmega     = 0.25*np.pi*((Apert*0.01)**2)*(((th_pix/3600)*np.pi/180.)**2) #m^2/sr
    dQ_rin_sh  = Inst['Bands']['dQ'][0][:]*np.sqrt(6.*Inst['Bands']['T_samp'][0][:]/t_int_sh)
    dQ_rin_dp  = Inst['Bands']['dQ'][0][:]*np.sqrt(6.*Inst['Bands']['T_samp'][0][:]/t_int_dp)
    #
    #  Define wavelength array for shallow survey
    lmin = Inst['Bands']['lmin'][0][:]
    lmax = Inst['Bands']['lmax'][0][:]
    lmed = np.sqrt(lmin*lmax)
    nlam = Inst['Bands']['R'][0][:]*np.log(lmax/lmin)
    nlam_arr = np.zeros(nbands)
    for ib in np.arange(nbands):
        lam_loc_arr = np.zeros(1)
        lam_loc_arr[0] = lmin[ib]
        lam_loc = 0.
        R_loc = R[ib]
        print 'ib R ',ib,R_loc
        while lam_loc < lmax[ib]:
            nlam_loc = lam_loc_arr.size
            lam_loc  = lam_loc_arr[nlam_loc-1]*(1.+1./R_loc)
            lam_loc_arr = np.append(lam_loc_arr,lam_loc)
        # By construction, the last element is always out of band so cut it off.
        lam_loc_arr  = lam_loc_arr[:lam_loc_arr.size-1]
        nlam_arr[ib] = lam_loc_arr.size
        if ib == 0:
            lam = lam_loc_arr
            lam_band = np.repeat([ib+1],lam_loc_arr.size)
        else:
            lam = np.append(lam,lam_loc_arr)
            lam_band = np.append(lam_band,np.repeat([ib+1],lam_loc_arr.size))
    # Arbitrarily cut of last freq element (imppse 96 bands)
    lam       = lam[0:lam.size-1]
    lam_band  = lam_band[0:lam_band.size-1]
    nlam = lam.size
    if verbose:
        print 'N lam : ',nlam
    #
    #  Define wavelength array for deep survey
    #  ie, keep full sampling in band 4
    #
    nlam_dp_arr = np.zeros(nbands)
    for ib in np.arange(nbands):
        lam_loc_arr = np.zeros(1)
        lam_loc_arr[0] = lmin[ib]
        lam_loc = 0.
        #if ib == 3:
        #    R_loc = R[0]
        #    ## ===> Chose redundancy for band 0 here.
        #else:
        #    R_loc = R[ib]
        R_loc = R[ib] # for v12 we have only one survey, ie step-size
        while lam_loc < lmax[ib]:
            nlam_loc = lam_loc_arr.size
            lam_loc  = lam_loc_arr[nlam_loc-1]*(1.+1./R_loc)
            lam_loc_arr = np.append(lam_loc_arr,lam_loc)
        # By construction, the last element is always out of band so cut it off.
        lam_loc_arr  = lam_loc_arr[:lam_loc_arr.size-1]
        nlam_dp_arr[ib] = lam_loc_arr.size
        if ib == 0:
            lam_dp = lam_loc_arr
            lam_dp_band = np.repeat([ib+1],lam_loc_arr.size)
        else:
            lam_dp = np.append(lam_dp,lam_loc_arr)
            lam_dp_band = np.append(lam_dp_band,np.repeat([ib+1],lam_loc_arr.size))
    # Arbitrarily cut of last freq element (imppse 96 bands)
    lam_dp      = lam_dp[0:lam_dp.size-1]
    lam_dp_band = lam_dp_band[0:lam_dp_band.size-1]
    nlam    = lam_dp.size
    nlam_dp = lam_dp.size
    if verbose:
        print 'N lam deep: ',nlam_dp
    # lam = np.append(lam,np.logspace(lmin[ib],lmax[ib],num=nlam[ib]))    # Why does not work?

    # Match wavelength to bins (not memory efficient but not an issue here)
    # (I don't handle out of band lambda but not an issue here)
    bins = np.append(lmin,lmax[nbands-1])
    #inds = np.digitize(lam, bins)-1 # This does now work when bin overlap...

    # Compute flux and brightness
    # ===========================

    # Compute i_photo
    sky_bkg = 6.7e3/lam**4/(np.exp(hc/(kb*5500.*lam))-1.) \
            + 5.120*1.e9/lam**4/(np.exp(hc/(kb*250.*lam))-1.) # in nW/m^2/sr
    sky_bkg_dp = 6.7e3/lam_dp**4/(np.exp(hc/(kb*5500.*lam_dp))-1.) \
            + 5.120*1.e9/lam_dp**4/(np.exp(hc/(kb*250.*lam_dp))-1.) # in nW/m^2/sr
    sky_bkg    *= ZL_fac ## Only for v4 and above in principle
    sky_bkg_dp *= ZL_fac ## Only for v4 and above in principle

    # Compute i_photo
    i_photo = 1.e-9*sky_bkg*AOmega[lam_band-1]*Inst['Bands']['eta_fpa'][0][lam_band-1]\
      *eta_opt[lam_band-1]/(R[lam_band-1]*hc/lam) # e/s
    i_photo_dp = 1.e-9*sky_bkg_dp*AOmega[lam_dp_band-1]*Inst['Bands']['eta_fpa'][0][lam_dp_band-1]\
      *eta_opt[lam_dp_band-1]/(R[lam_dp_band-1]*hc/lam_dp) # e/s

    # nIn [nW/m^2/sr/pixel]
    DC = 0.
    if ('DC' in Inst['Bands']): # Need for version => v>=13 (Survey Inefficiency)
        DC = Inst['Bands']['DC'][0][lam_band-1]
    dnIn_ppix_sh = np.sqrt((i_photo+DC)*t_int_sh+dQ_rin_sh[lam_band-1]**2)\
      /t_int_sh*np.sqrt(1./Inst['ShRed'][0])/i_photo*sky_bkg/np.sqrt(Tmission)
    dnIn_ppix_rn_sh = np.sqrt(DC*t_int_sh+dQ_rin_sh[lam_band-1]**2)/t_int_sh*np.sqrt(1./Inst['ShRed'][0])\
        /i_photo*sky_bkg/np.sqrt(Tmission) ## Read noise only
    DC = 0.
    if ('DC' in Inst['Bands']): # Need for version => v>=13 (Survey Inefficiency)
        DC = Inst['Bands']['DC'][0][lam_dp_band-1]
    dnIn_ppix_dp = np.sqrt((i_photo_dp+DC)*t_int_dp+dQ_rin_dp[lam_dp_band-1]**2)/t_int_dp*np.sqrt(1./Red_dp[0])\
        /i_photo_dp*sky_bkg_dp/np.sqrt(Tmission)
    dnIn_ppix_rn_dp = np.sqrt(DC*t_int_dp+dQ_rin_dp[lam_dp_band-1]**2)/t_int_dp*np.sqrt(1./Red_dp[0])\
        /i_photo_dp*sky_bkg_dp/np.sqrt(Tmission) ## Read noise only

    # Flux in shall [uJy] (one sigma using  Nps pixels, for optimal photometry)
    nps   = nps_0[lam_band-1] + (lam/lam_pix)**2
    dF_sh = np.sqrt(nps)*1.e-9*1.e26*1.e6*((np.pi/180.)*(th_pix[lam_band-1]/3600.))**2*dnIn_ppix_sh*(lam/c0) #muJy
    dF_rn_sh = np.sqrt(nps)*1.e-9*1.e26*1.e6*((np.pi/180.)\
             *(th_pix[lam_band-1]/3600.))**2*dnIn_ppix_rn_sh*(lam/c0) #muJy (Read noise only)
    nps_dp = nps_0[lam_dp_band-1] + (lam_dp/lam_pix)**2
    dF_dp  = np.sqrt(nps_dp)*1.e-9*1.e26*1.e6*\
      ((np.pi/180.)*(th_pix[lam_dp_band-1]/3600.))**2*dnIn_ppix_dp*(lam_dp/c0) #muJy
    dF_rn_dp  = np.sqrt(nps_dp)*1.e-9*1.e26*1.e6*\
      ((np.pi/180.)*(th_pix[lam_dp_band-1]/3600.))**2*dnIn_ppix_rn_dp*(lam_dp/c0) #muJy ## Read noise only
    #il=0;print lam[il],dF_sh[il]
    #il=80;print lam[il],dF_sh[il]
    #assert(0)

    # Magnitude AB (five sigma)
    Nsig   = 5
    Mab_sh = -2.5*np.log10(Nsig*1.e-6*dF_sh/3631.)
    Mab_dp = -2.5*np.log10(Nsig*1.e-6*dF_dp/3631.)

    # Added \delta I_\nu in [kJy/sr] for the deep survey
    dIn_ppix_dp = dnIn_ppix_dp*1.e-9*1.e23*lam_dp/c0

    # Added \delta F in [erg/cm^2s (3.5sig) for shallow and Euclid
    dF_3p5_sh = dF_sh*1.e-26*(1.e-6*c0/lam)*1.e-4*1.e7*3.5/41.5
    # Watch out (hard-coded R here)
    #=R5*1E-26*0.000001*(300000000000000/N5)*0.0001*10000000*3.5*(1/41.5)
    dF_Euclid = dF_3p5_sh*np.sqrt((2*0.56)/(7*0.61))*np.sqrt(15./40.)*np.sqrt(1.81/6.4)

    # Returned derived characteristic
    der_char = {'th_pix':(th_pix,'arcsec.'),'th_im_x':(th_im_x,'arcmin.'),\
                'th_im_y':(th_im_y,'deg.'),
                'spec_fov':(spec_fov,'sq. deg.'),'steps':(steps,'#'),'num_orbits':(num_orbits,'# per year'),\
                'num_step':((num_step_fs_sh,num_step_dp,num_step_gal),'# py'),'f_sh':(f_sh,''),\
                't_obs_sh':((t_obs_sh,t_obs_dp,0.),'s'),'t_int':((t_int_sh,t_int_dp,0.),'s.'),'eta_opt':(eta_opt,''),\
                'eta_tot':(eta_tot,''),'AOmega':(AOmega,'m^2/sr'),'dQ_rin':((dQ_rin_sh,dQ_rin_dp,0.),'')}

    # Print summpary
    if verbose:
        print '===='
        print 'Pixel: ',th_pix,' [arcsec.]'
        print 'CCD fov (width) is ',th_im,' [deg.]'
        print 'Channel element on the sky: ',th_im_x,' [arcmin.]'
        print 'One spectral element fov (width): ',spec_fov,' [deg.]'
        print 'Number of steps per fundamental length: ',steps,' '
        print 'Number of orbits ',num_orbits,' per year'
        print 'Number of dedicated steps for the fs shallow survey ',num_step_fs_sh,' per year'
        print 'Number of dedicated steps for the deep survey ',num_step_dp,' per year'
        print 'Number of dedicated steps for the galactic survey ',num_step_gal,' per year'
        print 'Number of small steps per orbit for the shallow survey ',num_sm_step_sh_po,' per orbit'
        print 'Effective redundancy for the deep survey',Red_dp
        print 'Number of required steps for the deep survey ', num_step_dp
        print 'Effective observing time per orbit for the shallow survey ',t_obs_sh,' s.'
        print 'Effective observing time per orbit for the shallow survey ',t_obs_sh/60.,' min.'
        print 'Effective observing time per orbit for the deep survey ',t_obs_dp,' s.'
        print 'Effective observing time per orbit for the deep survey ',t_obs_dp/60.,' min.'
        print 'Integration time per exposure for the fs shallow survey ',t_int_sh,' s.'
        print 'Integration time per exposure for the deep survey ',t_int_dp,' s.'
        print 'Effective number of pixels for optimal photometry ', nps_0[:] + (lmed/lam_pix)**2
        print 'Total efficiency ',eta_tot
        print 'Total throughput ',AOmega
        #print 'Total data rate per orbit '
        print '===='

        print '===='
        il  = 40#nlam-1
        iil = 75
        print 'Lambda        : {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(lam[0],lam[il],lam[iil])
        print 'Sky_bkg       : {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(sky_bkg[0],sky_bkg[il],sky_bkg[iil])
        print 'Iphoto        : {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(i_photo[0],i_photo[il],i_photo[iil])
        print 'dnIn_ppix (sh): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(dnIn_ppix_sh[0],dnIn_ppix_sh[il],dnIn_ppix_sh[iil])
        print 'dF        (sh): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(dF_sh[0],dF_sh[il],dF_sh[iil])
        print 'dF_3p5    (sh): {0:6.3e}, {1:6.3e}, {2:6.3e}'.format(dF_3p5_sh[0],dF_3p5_sh[il],dF_3p5_sh[iil])
        print 'dF_Euc    (sh): {0:6.3e}, {1:6.3e}, {2:6.3e}'.format(dF_Euclid[0],dF_Euclid[il],dF_Euclid[iil])
        print 'M_AB      (sh): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(Mab_sh[0],Mab_sh[il],Mab_sh[iil])
        print 'dnIn_ppix (dp): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(dnIn_ppix_dp[0],dnIn_ppix_dp[il],dnIn_ppix_dp[iil])
        print 'dIn_ppix  (dp): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(dIn_ppix_dp[0],dIn_ppix_dp[il],dIn_ppix_dp[iil])
        print 'dF        (dp): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(dF_dp[0],dF_dp[il],dF_dp[iil])
        print 'M_AB      (dp): {0:6.3f}, {1:6.3f}, {2:6.3f}'.format(Mab_dp[0],Mab_dp[il],Mab_dp[iil])
        print '===='

        print '===='
        print 'Total number of frequency elements (shallow): ',nlam
        print 'Total number of frequency elements (deep)   : ',nlam_dp
        print 'Number of frequency elements per band (shallow): ',nlam_arr
        print 'Number of frequency elements per band (deep): ',nlam_dp_arr
        print '===='

    Sens = {'lam':(lam,'mum'),'lam_band':(lam_band,'band'),'sky_bkg':(sky_bkg,'nW/m^2/sr'),'i_photo':(i_photo,'e/s'),\
            'nIn_sh':(dnIn_ppix_sh,'nW/m^2/sr/pix'),'nIn_rn_sh':(dnIn_ppix_rn_sh,'nW/m^2/sr/pix'),\
            'nIn_dp':(dnIn_ppix_dp,'nW/m^2/sr/pix'),'nIn_rn_dp':(dnIn_ppix_rn_dp,'nW/m^2/sr/pix'),\
            'dF_sh':(dF_sh,'uJy'),'Mab_sh':(Mab_sh,''),'dF_dp':(dF_dp,'uJy'),'Mab_dp':(Mab_dp,''),\
            'dF_rn_sh':(dF_rn_sh,'uJy'),'dF_rn_dp':(dF_rn_dp,'uJy'),\
            'der_char':der_char,'N_eff':(nps,'Effective Npix'),'lam_dp':(lam_dp,'mum'),'lam_dp_band':(lam_dp_band,'band'),\
            'dIn_dp':(dIn_ppix_dp,'kJy/sr/pix'),'dF_3p5_sh':(dF_3p5_sh,'erg/cm^2 s'),'dF_3p5_Euc':(dF_Euclid,'erg/cm^2 s')}

    return Sens

#! /bin/bash

# This script normalizes the photometry and creates the final simulated photometry catalog,
# which can then be used by MakeImage.sh to generate images.

source ./SPHEREx.params

source $SPHEREX_ROOT/src/scripts/utils.sh

errlog "Re-organizing the simulated photometry data."

# This saves its output to renorm.log and a collection of files in $SimulationCatalogDir
PrepareFluxes.pl $SimulatedDataFilterList $OutputModel $NoiseModel

errlog "Making the simulated catalog."

# This reads the data that was manipulated and output by PrepareFluxes.pl, including renorm.log.
make_input_ground_cat.pl $ExternalDataFilterList $SimulationCatalogDir $RawInputCatalog > $ModelParamCatalog

errlog "The simulated photometry catalog has been saved to $ModelParamCatalog."
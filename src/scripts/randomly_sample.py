#! /usr/bin/python

# randonly selects lines of a text file (the model grid, for instance) and prints out the
# randomly-selected lines to standard output.

import random
from sys import stdout, stderr, argv

if len(argv) != 3:
    stderr.write("Usage: " + argv[0] + " output_grid_size input_file" )
    exit(1)

number_of_entries_in_sample_grid = int(argv[1])
filename = argv[2]

grid = open(filename)

grid_contents = [line for line in grid]

grid_size = len(grid_contents)

if number_of_entries_in_sample_grid >= grid_size:
    stderr.write("You would be better off just using the full grid, since your desired grid is not smaller than the input grid")
    exit()

for i in range(number_of_entries_in_sample_grid):
    stdout.write(grid_contents[random.randint(0, grid_size-1)])

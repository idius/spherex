#!/usr/bin/perl

$usage = "$0 ground_filter_list catalog_directory input_catalog\n";

$flist = shift(@ARGV) || die $usage;
$catdir = shift(@ARGV) || die $usage;
$catalog = shift(@ARGV) || die $usage;

$Nf=0;
open(FDATA," < $flist");

$catcmd="pastecats ";
$outphot = "";

# get filter names and filter flux filenames

while(<FDATA>){
  chomp;
  ($fname, $ffile) = split;
 
  $datafile = $catdir . "/" . $ffile . ".txt";
  $datacat = $catdir . "/" . $fname . ".bin";

  system("lc -C -n ID -n RA -n DEC -n redshift -n $fname < $datafile | lc -b > $datacat");

  $outphot .= " -n " . $fname;
  
  $catcmd .= " " . $datacat;
  $Nf++;
}
close(FDATA);

$catcmd .= " > $catdir/ground.phot.bin";
$outphot .= " ";

printf STDERR "Running cat command: %s\n",$catcmd;
system($catcmd); # stores data in ground.phot.bin

# get simulated ground-based photometry data for filters G, R, I, Z, Y, Wise1, Wise2

@IDs=();
open(GPHOT,"lc -o < $catdir/ground.phot.bin | ");
while(<GPHOT>)
{
  chomp;
  @data=split;

  $ID = shift(@data);
  $ID = int($ID);

  $Gdata{$ID}=[@data];

  push(@IDs,$ID);
}
close(GPHOT);

# get the RA, DEC, I-band magnitude, redshift, template number, E(B-V), reddening law

open(CINFO,"< $ENV{'InputModels'} ");
while(<CINFO>)
{
  chomp;
  @data=split;
  
  $ID = shift(@data);
  $ID = int($ID);

  $Cdata{$ID}=[@data];
}
close(CINFO);

# get mass and nband data from the raw catalog

open(MASS," < $catalog");
while(<MASS>)
{
  chomp;
  @data = split;
  
  $ID = shift(@data);
  $ID = int($ID);
  $mass = shift(@data);
  $Nb = shift(@data);

  $Mdata{$ID} = $mass;
  $Nbands{$ID} = $Nb;
}
close(MASS);

# get the mean flux, S/N, and chisq values, computed by PrepareFluxes.pl

open(NLOG,"lc -o ID meanflux SNR chisq < renorm.log |");

while(<NLOG>)
{
  chomp;
  @data=split;
  
  $ID = shift(@data);
  $ID = int($ID);

  $NLOGdata{$ID}=[@data];
}
close(NLOG);

# write the simulated ground catalog

open(OUTPUT,"| lc -C -n ID -n logMass -n Nbands -n RA -n DEC -n i_input -n redshift -n SED -n eBV -n Rlaw -n meanflux -n SNR -n chisq $outphot ");

foreach $ID (@IDs){

  # ID, logmass, nbands

  printf OUTPUT "%d ",$ID;
  printf OUTPUT "%s ",$Mdata{$ID};
  printf OUTPUT "%s ",$Nbands{$ID};

  # RA, DEC, I-band magnitude, redshift, template number, E(B-V), reddening law

  for($i = 0; $i < 7; $i++)
  {
    printf OUTPUT "%s ",$Cdata{$ID}[$i];
  }

  # mean flux, S/N, chisq

  for($i = 0; $i < 3; $i++)
  {
    printf OUTPUT "%s ", $NLOGdata{$ID}[$i];
  }

  # filters (G, R, I, Z, Y, Wise1, Wise2)

  for($i = 3; $i < ($Nf + 3); $i++)
  {
    printf OUTPUT "%s ",$Gdata{$ID}[$i];
  }
  printf OUTPUT "\n";
}
close(OUTPUT);
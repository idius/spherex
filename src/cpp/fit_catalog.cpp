/*! \file

Contains the main function for the catalog photometry fitting code (fitcat).
*/

#include "grid_tools.hpp"

#include <cstdlib>
#include <string>
#include <iostream>


int main(int argc, char* argv[])
{
    // usage messages:

    std::string mkgrid_usage = std::string(argv[0])
                             + std::string(" mkgrid catalog filter_list template_list output_file");

    std::string fit_usage = std::string(argv[0]) + std::string(" fit catalog grid output_file");

    if (argc < 5 || argc > 6)
    {
        std::cerr << "\nUsage: " << mkgrid_usage + "\n\nOR     " << fit_usage + "\n\n";
        exit(1);
    }

    std::string  task_str(argv[1]);

    if (task_str == "fit")
    {
        if (argc != 5) { std::cerr << "\nUsage: " << fit_usage << "\n"; exit(1); }

        const char* catalog_filename = argv[2];
        const char* model_grid_filename = argv[3];
        const char* output_filename = argv[4];

        FindBestFits(catalog_filename, model_grid_filename, output_filename);
    }
    else if (task_str == "mkgrid")
    {
        if (argc != 6) { std::cerr << "\nUsage: " << mkgrid_usage << "\n"; exit(1); }

        const char* catalog_filename = argv[2];
        const char* filterlist_filename = argv[3];
        const char* templatelist_filename = argv[4];
        const char* output_filename = argv[5];

        MakePhotometryFittingGrid(catalog_filename,
                                  filterlist_filename,
                                  templatelist_filename,
                                  output_filename);
    }
    else
    {
        std::cerr << "Error: The task must be 'fit' or 'mkgrid.' You entered " << task_str << ".\n";
        exit(1);
    }

    return 0;
}




